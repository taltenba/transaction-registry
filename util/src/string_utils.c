/**
 * \file
 * \brief 	Utility routines related to strings
 * \date    Created:  11/02/2017
 * \date    Modified: 01/01/2018
 * \author  ALTENBACH Thomas (thomas.altenbach@utbm.fr)
 */

#include <stdlib.h>
#include <ctype.h>
#include <errno.h>
#include <limits.h>
#include <float.h>
#include <stdio.h>
#include <assert.h>
#include <string_utils.h>

#define DATE_SEP '-'    /**< Date separator */
#define TIME_SEP ':'    /**< Time separator */

size_t find_nonspace(char const* str)
{
    size_t i = 0;
    
    assert(str != NULL);

    while (isspace(str[i]))
        ++i;

    return i;
}

BOOL is_blank(char const* str, size_t* begin)
{
    size_t first = find_nonspace(str);
    
    assert(str != NULL);

    if (begin != NULL)
        *begin = first;

    return str[first] == '\0';
}

BOOL parse_bool(char const* str, BOOL* result)
{
    int value;
    
    assert(str != NULL);
	assert(result != NULL);

    if (!parse_int(str, &value))
        return FALSE;

    if (value != 0 && value != 1)
        return FALSE;

    *result = (BOOL) value;
    
    return TRUE;
}

BOOL parse_char(char const* str, char* result)
{
    size_t first_char_ind;
    long value;
    char* end;
    
    assert(str != NULL);
	assert(result != NULL);
	
    if (is_blank(str, &first_char_ind)) /* Empty string */
        return FALSE;

    value = strtol(&str[first_char_ind], &end, 10);
    
    if (errno == ERANGE || value < CHAR_MIN || value > CHAR_MAX || !is_blank(end, NULL))
    	return FALSE;
    	
    *result = (char) value;
    
    return TRUE;
}

BOOL parse_int(char const* str, int* result)
{
    size_t first_char_ind;
    long value;
    char* end;
    
    assert(str != NULL);
	assert(result != NULL);

    if (is_blank(str, &first_char_ind)) /* Empty string */
        return FALSE;

    value = strtol(&str[first_char_ind], &end, 10);
    
    if (errno == ERANGE || value < INT_MIN || value > INT_MAX || !is_blank(end, NULL))
    	return FALSE;
    	
    *result = (int) value;
    
    return TRUE;
}

BOOL parse_long(char const* str, long* result)
{
    size_t first_char_ind;
    long value;
    char* end;
    
    assert(str != NULL);
	assert(result != NULL);

    if (is_blank(str, &first_char_ind)) /* Empty string */
        return FALSE;
    
    value = strtol(&str[first_char_ind], &end, 10);

	if (errno == ERANGE || !is_blank(end, NULL))
    	return FALSE;
    	
    *result = (long) value;
    
    return TRUE;
}

BOOL parse_uchar_hex(char const* str, unsigned char* result)
{
    size_t first_char_ind;
    unsigned long value;
    char* end;
    
    assert(str != NULL);
	assert(result != NULL);

	first_char_ind = find_nonspace(str);

    if (str[first_char_ind] == '\0' || str[first_char_ind] == '-')  /* Empty string or negative number */
        return FALSE;

    value = strtoul(&str[first_char_ind], &end, 16);

	if (errno == ERANGE || value > UCHAR_MAX || !is_blank(end, NULL))
    	return FALSE;
    	
    *result = (unsigned char) value;
    
    return TRUE;
}

BOOL parse_uint(char const* str, unsigned int* result)
{
    size_t first_char_ind;
    unsigned long value;
    char* end;
    
    assert(str != NULL);
	assert(result != NULL);

	first_char_ind = find_nonspace(str);

    if (str[first_char_ind] == '\0' || str[first_char_ind] == '-')  /* Empty string or negative number */
        return FALSE;

    value = strtoul(&str[first_char_ind], &end, 10);

	if (errno == ERANGE || value > UINT_MAX || !is_blank(end, NULL))
    	return FALSE;
    	
    *result = (unsigned int) value;
    
    return TRUE;
}

BOOL parse_ulong(char const* str, unsigned long* result)
{
    size_t first_char_ind;
	unsigned long value;
    char* end;
    
    assert(str != NULL);
	assert(result != NULL);
    
    first_char_ind = find_nonspace(str);
    
    if (str[first_char_ind] == '\0' || str[first_char_ind] == '-') /* Empty string or negative number */
        return FALSE;
    
    value = strtoul(&str[first_char_ind], &end, 10);

    if (errno == ERANGE || !is_blank(end, NULL))
    	return FALSE;
    	
    *result = (unsigned long) value;
    
    return TRUE;
}

BOOL parse_float(char const* str, float* result)
{
    size_t first_char_ind;
    double value;
    char* end;
    
    assert(str != NULL);
	assert(result != NULL);

    if (is_blank(str, &first_char_ind)) /* Empty string */
        return FALSE;

    value = strtod(&str[first_char_ind], &end);

	if (errno == ERANGE || value < FLT_MIN || value > FLT_MAX || !is_blank(end, NULL))
    	return FALSE;
    
    *result = (float) value;
    
    return TRUE;
}

BOOL parse_double(char const* str, double* result)
{
    size_t first_char_ind;
    double value;
    char* end;
    
    assert(str != NULL);
	assert(result != NULL);
	
    if (is_blank(str, &first_char_ind)) /* Empty string */
        return FALSE;

    value = strtod(&str[first_char_ind], &end);

    if (errno == ERANGE || !is_blank(end, NULL))
    	return FALSE;
    	
    *result = (double) value;
    
    return TRUE;
}

/**
 * \brief Converts a string to a integer number, stopping at the first character being either
 *        the specified separator or the terminating null character.
 * \param[in] str The string
 * \param[out] result The result of the function on success
 * \param[in] min Lower bound above which the result must be (inclusive)
 * \param[in] max Upper bound under which the result must be (inclusive)
 * \param[in] max_length Maximum number of characters read by this function (excluding the 
 *                       stop character, > 0)
 * \param[in] sep The separator
 * \return Number of characters read on success (including the stop character, > 0), 0 otherwise
 *
 * The operation succeed iff :
 *     - The string is not empty.
 *     - Contains only (until the stop character) an integer number between llim and ulim.
 *
 * On failure, nothing is written into result and *str is not modified.
 */
static size_t parse_date_time_part(char const* str, int* result, int min, int max, unsigned int max_length, char sep)
{
    int value = 0;
    unsigned int i = 0;

    while (i < max_length && isdigit(*str) && *str != sep && *str != '\0')
    {
        value = value * 10 + (*str++ - '0');
        ++i;
    }

    if (*str != sep && !is_blank(str, NULL))
        return 0;

    if (value < min || value > max)
        return 0;

    *result = value;
    return i + 1;
}

/**
 * \brief Parse the next date time part of a string
 */
#define PARSE_NEXT_DATE_TIME_PART(str, result, min, max, max_length, sep) \
        do\
        {\
            read_count = parse_date_time_part(str, result, min, max, max_length, sep);\
            str += read_count;\
            \
            if (read_count == 0 || *str == '\0')\
        		return FALSE;\
        } while (0)

BOOL parse_date_time(char const* str, time_t* result)
{
	int year, month, day, hours, minutes, seconds;
	int read_count;
	char buffer[3];
	BOOL has_time;
	time_t tmp;
	struct tm date_time = {0};
	
	assert(str != NULL);
	assert(result != NULL);
	
    str += find_nonspace(str);
	
	PARSE_NEXT_DATE_TIME_PART(str, &year, 0, 9999, 4, DATE_SEP);
	
	PARSE_NEXT_DATE_TIME_PART(str, &month, 1, 12, 2, DATE_SEP);
	
	if (str[0] == '\0' || str[1] == '\0')
		return FALSE;
	
	buffer[0] = str[0];
	buffer[1] = str[1];
	buffer[2] = '\0';
	
	has_time = !is_blank(&str[2], NULL);
	
	read_count = parse_date_time_part(buffer, &day, 1, 31, 2, DATE_SEP);
    
	if (read_count == 0)
    	return FALSE;
    
    if (has_time)
    {
    	str += read_count;
    	
    	PARSE_NEXT_DATE_TIME_PART(str, &hours, 0, 23, 2, TIME_SEP);
    	
    	PARSE_NEXT_DATE_TIME_PART(str, &minutes, 0, 59, 2, TIME_SEP);
    	
        read_count = parse_date_time_part(str, &seconds, 0, 59, 2, TIME_SEP);
        str += read_count - 1;
            
        if (read_count == 0 || *str == TIME_SEP)
    		return FALSE;
        	
        date_time.tm_hour = hours;
        date_time.tm_min = minutes;
        date_time.tm_sec = seconds;
    }
    
    date_time.tm_mday = day;
    date_time.tm_mon = month - 1;
    date_time.tm_year = year - 1900;

    tmp = mktime(&date_time);

    if (tmp == -1)
        return FALSE;

    *result = tmp;
    return TRUE;
}

void print_date_time(time_t date_time, char* str)
{
	assert(str != NULL);
	strftime(str, 20, "%Y-%m-%d %H:%M:%S", localtime(&date_time));
}
