/**
 * \file
 * \brief   Sorted generic map matching postive integer keys with values using a skip-list
 * \date    Created: 11/05/2017
 * \date    Modified: 26/12/2017
 * \author  ALTENBACH Thomas (thomas.altenbach@utbm.fr)
 *
 * Entries are stored in ascending order of the keys, enabling that way sorted traversing
 * of the map.
 *
 * Implemented over a skip-list, this data structure guarantee therefore on average similar
 * performance as a self-balancing tree-based map, that is to say logarithmic complexity for searches,
 * insertions, and removals. 
 * 
 * Storing on average only 4/3 pointers per nodes, in addition to the data, this data structure also 
 * use less memory than a typical tree-map implementation.
 *
 * Moreover, insertions and removals in a skip-list are often less expensive than in a self-balancing 
 * tree since neither traversal nor rebalancing is needed.
 * In particular, when those operations are performed in ascending or descending order, they are
 * completed in constant time by the skip-list.
 *
 * However, by the probabilistic nature of the skip-list, performance is less stable than in a 
 * tree-map and the logarithmic complexity of the searches, insertions and removals is only 
 * guaranteed on average, although it is extremely unlikely that the real performance moves far away 
 * from the average one (and more the skip-list contains elements, more the likelihood decreases).
 */

#include <stdlib.h>
#include <assert.h>
#include <ulskip_map.h>
#include <rnd_utils.h>
#include <math.h>
#include <math_utils.h>
#include <limits.h>

/**
 * \brief Integer q such that p = 1/(2^q), with p the fraction of nodes present at the i-th level that 
 *        are also present at the (i+1)-th level.
 *
 * We usually choose p = 1/2 or p = 1/4. Search time is almost the same for both values, but with 
 * the latter only 4/3 forward pointers are stored on average in a node, against two pointers per 
 * nodes with the former. However the variability of search time is lower with p = 1/2 than with 
 * p = 1/4.
 */
#define NEXT_LEVEL_PROB 2

/**
 * \brief Smallest maximum number of levels in an ULSkipMap
 */
#define MIN_LEVELS_COUNT 2

/**
 * \brief Number of levels added to the maximum one in an ULSkipMap each time the maximum is exceeded.
 *		  Defines also the default maximum number of levels when a new ULSkipMap is initialized.
 */
#define MAX_LEVELS_COUNT_INC 4

/**
 * \brief Size of memory pool chunks (in bytes)
 */
#define MEMPOOL_CHUNK_SIZE 4096

/**
 * \brief Maximum number of levels in an ULSkipMap
 */
#define MAX_LEVELS_COUNT (MEMPOOL_CHUNK_SIZE - sizeof(ULSkipNode)) / sizeof(ULSkipNode*) + 1

struct ulskipnode
{
    unsigned long key;          	/**< Key of the node */
    void* value;                	/**< Value of the node */
    struct ulskipnode* prev;        /**< Previous node on the first level */
    struct ulskipnode* next[1];   	/**< Next nodes in the list indexed by the layer */
};

/**
 * \brief Indicates whether or not the specified node is the header of a map
 * \param[in] node The node
 * \return TRUE if the node is the header of a map, FALSE otherwise
 */
static BOOL is_head(ULSkipNode* node)
{
	return node->prev == NULL;
}

/**
 * \brief Allocates a new node for a ULSkipMap
 * \param[in] map The map
 * \param[in] level Level of the node (the first level has the index zero)
 * \return The newly created node on success, NULL otherwise (out of memory)
 */
static ULSkipNode* alloc_node(ULSkipMap* map, int level)
{
    return fsmpool_alloc(&map->nodes_pools[level]);
}

/**
 * \brief Initializes the key, value and prev fields of a node
 * \param[out] node The node to intialize
 * \param[in] key The key of the nodes
 * \param[in] value The value of the node
 * \param[in] prev The previous node
 */
static void init_node(ULSkipNode* node, unsigned long key, void* value, ULSkipNode* prev)
{
    node->key = key;
    node->value = value;
    node->prev = prev;
}

/**
 * \brief Allocates and initializes the head node of a ULSkipMap
 * \param[in] map The map
 * \return TRUE on success, FALSE otherwise (out of memory)
 */
static BOOL init_head(ULSkipMap* map)
{
	int i;
    ULSkipNode* head;
    
    head = alloc_node(map, map->max_level - 1);

    if (head == NULL)
        return FALSE;

    head->key = 0UL;
    head->value = NULL;
    head->prev = NULL;

    for (i = map->max_level - 1; i >= 0; --i)
        head->next[i] = NULL;

    map->head = head;

    return TRUE;
}

/**
 * \brief Initializes the memory pools of an ULSkipMap, starting at the specified level index
 * \param[in] map The map
 * \param[in] start Index of the first memory pool to initialize (inclusive)
 * \param[in] end End memory pool index (exclusive)
 */
static void init_mempools(ULSkipMap* map, int start, int end)
{
	int i;
	size_t cur_node_size;
	
	cur_node_size = sizeof(ULSkipNode) + start * sizeof(ULSkipNode*);
	
	for (i = start; i < end; ++i)
	{
		fsmpool_init(&map->nodes_pools[i], cur_node_size, MEMPOOL_CHUNK_SIZE / cur_node_size, 0);
		cur_node_size += sizeof(ULSkipNode*);
	}
}

/**
 * \brief Clears all the memory pools of a ULSkipMap
 * \param[in] map The map
 */
static void clear_mempools(ULSkipMap* map)
{
	int i;
	
	for (i = map->max_level - 1; i >= 0; --i)
		fsmpool_clear(&map->nodes_pools[i]);
}

BOOL ulsmap_init(ULSkipMap* map, size_t init_size)
{
	int i;
	int max_level;
	ULSkipNode* head;
	
    assert(map != NULL);
    
    if (init_size == 0)
    	max_level = MAX_LEVELS_COUNT_INC;
    else
    	max_level = ilog_ceil(init_size, NEXT_LEVEL_PROB);
    	
    if (max_level < MIN_LEVELS_COUNT)
		max_level = MIN_LEVELS_COUNT;
	
	map->max_level = max_level;
	map->nodes_pools = malloc(max_level * sizeof(FSMemPool));
	
	if (map->nodes_pools == NULL)
		return FALSE;
	
	init_mempools(map, 0, max_level);
	
    if (!init_head(map))
    {
    	clear_mempools(map);
    	free(map->nodes_pools);
    	return FALSE;
    }
    
    map->tail_nodes = malloc(sizeof(ULSkipNode*) * max_level);
    
    if (map->tail_nodes == NULL)
    {
    	clear_mempools(map);
    	free(map->nodes_pools);
    	return FALSE;
    }
    
    map->prevs_buffer = malloc(sizeof(ULSkipNode*) * max_level);
    
    if (map->prevs_buffer == NULL)
    {
    	clear_mempools(map);
    	free(map->nodes_pools);
    	free(map->tail_nodes);
    	return FALSE;
    }
    
    head = map->head;
    
    for (i = 0; i < max_level; ++i)
    {
    	map->tail_nodes[i] = head;
    	map->prevs_buffer[i] = head;
    }
    
    map->size = 0;
    map->last_level = 0;

    return TRUE;
}

void ulsmap_it_init(ULSkipMap const* map, ULSkipMapIterator* it)
{
    assert(map != NULL);
    assert(it != NULL);
    
    it->start_key = 0;
    it->end_key = ULONG_MAX;
    it->cur = map->head;
}

BOOL ulsmap_it_has_next(ULSkipMapIterator const* it)
{
	assert(it != NULL);
    return it->cur->next[0] != NULL && it->cur->next[0]->key <= it->end_key;
}

BOOL ulsmap_it_has_prev(ULSkipMapIterator const* it)
{
	assert(it != NULL);
	return !is_head(it->cur) && it->cur->key >= it->start_key;
}

void* ulsmap_it_next(ULSkipMapIterator* it)
{
	ULSkipNode* next;
	
    assert(it != NULL);
    assert(ulsmap_it_has_next(it));
	
	next = it->cur->next[0];
    it->cur = next;

    return next->value;
}

void* ulsmap_it_prev(ULSkipMapIterator* it)
{
	ULSkipNode* prev;
	
	assert(it != NULL);
	assert(ulsmap_it_has_prev(it));
	
	prev = it->cur;
	it->cur = prev->prev;
	
	return prev->value;
}

/**
 * \brief Finds the node having the specified key in a ULSkipMap, or if the map does not contain that key, the last node having a strictly lower key value
 * \param[in] map The map
 * \param[in] key The key to search for
 * \param[out] found The found node
 * \return TRUE if a node having the specified key has been found, FALSE otherwise
 */
static BOOL find_node_or_prev(ULSkipMap const* map, unsigned long key, ULSkipNode** found)
{
	int i;
    ULSkipNode* cur;
    
    if (key >= map->tail_nodes[0]->key)
    {
    	*found = map->tail_nodes[0];
    	return *found != map->head && (*found)->key == key;
    }
    
    cur = map->head;

    for (i = map->last_level; i >= 0; --i)
    {
        while (cur->next[i] != NULL && cur->next[i]->key < key)
        	cur = cur->next[i];
    }

    if (cur->next[0] != NULL && cur->next[0]->key == key)	/* The key has been found */
    {
        *found = cur->next[0];
        return TRUE;
    }
    else
    {
        *found = cur;
        return FALSE;
    }
}

/**
 * \brief Finds the node having the specified key if the latter exists and, on all levels, the last node having a strictly lower key value
 * \param[in] map The map
 * \param[in] key The key to search for
 * \return The node having the specified key if it has been found, NULL otherwise
 *
 * The previous nodes on all levels are stored in the prevs_buffer array of the map. That array is 
 * indexed by level and contains, for each level, the last node having a key value strictly lower 
 * than the one specified.
 */
static ULSkipNode* find_prevs(ULSkipMap const* map, unsigned long key)
{
	int i;
    ULSkipNode* cur;
    
    if (key > map->tail_nodes[0]->key)
    {
    	for (i = map->last_level; i >= 0; --i)
    		map->prevs_buffer[i] = map->tail_nodes[i];
    		
    	return NULL;
    }
    
    cur = map->head;

    for (i = map->last_level; i >= 0; --i)
    {
        while (cur->next[i] != NULL && cur->next[i]->key < key)
            cur = cur->next[i];

        map->prevs_buffer[i] = cur;
    }

    cur = cur->next[0];

    if (cur == NULL || cur->key != key)    /* The key has not been found */
        return NULL;

    return cur;
}

void* ulsmap_get_value(ULSkipMap const* map, unsigned long key)
{
	ULSkipNode* found_node;
	
    assert(map != NULL);

    if (!find_node_or_prev(map, key, &found_node))    /* The key has not been found */
        return NULL;

    return found_node->value;
}

void ulsmap_get_from(ULSkipMap const* map, unsigned long min, ULSkipMapIterator* it)
{
	assert(map != NULL);
	assert(it != NULL);
	
	ulsmap_get_between(map, min, ULONG_MAX, it);
}

void ulsmap_get_to(ULSkipMap const* map, unsigned long max, ULSkipMapIterator* it)
{
	assert(map != NULL);
	assert(it != NULL);
	
	ulsmap_get_between(map, 0UL, max, it);
}

void ulsmap_get_between(ULSkipMap const* map, unsigned long min, unsigned long max, 
                        ULSkipMapIterator* it)
{
	ULSkipNode* start_node;
	
	assert(map != NULL);
	assert(it != NULL);
	assert(min <= max);
	
	it->start_key = min;
	it->end_key = max;
	
	if (find_node_or_prev(map, min, &start_node))
		start_node = start_node->prev;
		
	it->cur = start_node;
}

void ulsmap_get_between_reverse(ULSkipMap const* map, unsigned long min, unsigned long max, 
                                ULSkipMapIterator* it)
{
	ULSkipNode* end_node;
	
	assert(map != NULL);
	assert(it != NULL);
	assert(min <= max);
	
	it->start_key = min;
	it->end_key = max;
	
	find_node_or_prev(map, max, &end_node);
		
	it->cur = end_node;
}

/**
 * \brief Chooses randomly a level for a new node of the specified ULSkipMap
 * \param[in] map The map
 * \return The randomly chosen level
 */
static int rnd_level(ULSkipMap const* map)
{
    int max_level;   /* Index of the maximum level that can be chosen */
    int level;
	
	max_level = (map->last_level + 1 < MAX_LEVELS_COUNT) ? map->last_level + 1 : MAX_LEVELS_COUNT - 1;
	level = 0;
	
    /* 
     * We go to the next level with a probability of 1 / (2^NEXT_LEVEL_PROB).
     * The level of the new node is at most the next level of the current last level
     * to avoid having a node which is very high in comparison with the others.
     */
    while (level < max_level && rnd_pow2(NEXT_LEVEL_PROB) == 0)
        ++level;

    return level;
}

/**
 * \brief Increases the maximum level of an ULSkipMap
 * \param[in] map The map
 * \return TRUE on success, FALSE otherwise (out of memory)
 *
 * The content of the previous nodes buffer of the ULSkipMap is preserved.
 */
static BOOL increase_max_level(ULSkipMap* map)
{
	int i;
	int new_max_level;
	ULSkipNode* old_head;
	ULSkipNode* new_head;
	ULSkipNode** new_tail_nodes;
	ULSkipNode** new_prevs_buffer;
	FSMemPool* new_nodes_pools;
	
	new_max_level = map->max_level + MAX_LEVELS_COUNT_INC;
	
	new_tail_nodes = realloc(map->tail_nodes, sizeof(ULSkipNode*) * new_max_level);
	
	if (new_tail_nodes == NULL)
		return FALSE;
	
	map->tail_nodes = new_tail_nodes;
	
	new_prevs_buffer = realloc(map->prevs_buffer, sizeof(ULSkipNode*) * new_max_level);
	
	if (new_prevs_buffer == NULL)
		return FALSE;
		
	map->prevs_buffer = new_prevs_buffer;
	
	new_nodes_pools = realloc(map->nodes_pools, sizeof(FSMemPool) * new_max_level);
	
	if (new_nodes_pools == NULL)
		return FALSE;
	
	map->nodes_pools = new_nodes_pools;
	
	init_mempools(map, map->max_level, new_max_level);
	
	new_head = alloc_node(map, new_max_level - 1);
	
	if (new_head == NULL)
	{
		for (i = map->max_level; i < new_max_level; ++i)
			fsmpool_clear(&map->nodes_pools[i]);
		
		return FALSE;
	}
	
	new_head->key = 0UL;
	new_head->value = NULL;
	new_head->prev = NULL;
	
	old_head = map->head;
	
	if (old_head->next[0] != NULL)
		old_head->next[0]->prev = new_head;
	
	i = map->last_level;
	
	while (i >= 0 && new_prevs_buffer[i] == old_head)
	{
		new_head->next[i] = old_head->next[i];
		new_prevs_buffer[i] = new_head;
		--i;
	}
	
	for (; i >= 0; --i)
		new_head->next[i] = old_head->next[i];
		
	for (i = map->last_level + 1; i < new_max_level; ++i)
	{
		new_head->next[i] = NULL;
		new_tail_nodes[i] = new_head;
		new_prevs_buffer[i] = new_head;
	}
	
	fsmpool_free(&map->nodes_pools[map->max_level - 1], old_head);
	
	map->head = new_head;
	map->max_level = new_max_level;
	
	return TRUE;
}

BOOL ulsmap_put(ULSkipMap* map, unsigned long key, void* value)
{
	int i;
	int level;
	ULSkipNode* key_node;
	ULSkipNode* new_node;
	ULSkipNode** tail_nodes;
	ULSkipNode** previous;
	
    assert(map != NULL);
    assert(value != NULL);
    
    tail_nodes = map->tail_nodes;
    
    if (tail_nodes[0] != map->head && key == tail_nodes[0]->key)
    	key_node = tail_nodes[0];
    else
    	key_node = find_prevs(map, key);
    
    /* The key has already a mapping in the list, we simply update the value */
	if (key_node != NULL)
	{
	    key_node->value = value;
	    return TRUE;
	}
	
    /* Otherwise, a new node has to be created and put in the map */
    level = rnd_level(map);
    
    if (level >= map->max_level)
    {
    	if (!increase_max_level(map))
    		return FALSE;
    		
    	tail_nodes = map->tail_nodes;
    }
    
    previous = map->prevs_buffer;
    
    new_node = alloc_node(map, level);

    if (new_node == NULL)
        return FALSE;
        
    init_node(new_node, key, value, previous[0]);

	if (previous[0]->next[0] != NULL)
    	previous[0]->next[0]->prev = new_node;

    for (i = 0; i <= level; ++i)
    {
        new_node->next[i] = previous[i]->next[i];
        previous[i]->next[i] = new_node;
    }
    
    i = level;
    
    while (i >= 0 && new_node->next[i] == NULL)
    {
    	tail_nodes[i] = new_node;
    	--i;
    }

    if (level > map->last_level)
        map->last_level = level;

    map->size++;

    return TRUE;
}

void* ulsmap_remove(ULSkipMap* map, unsigned long key)
{
	int i;
	void* removed_value;
	ULSkipNode* key_node;
	ULSkipNode** previous;
	
    assert(map != NULL);

	key_node = find_prevs(map, key);
    
    if (key_node == NULL)
    	return NULL;
    	
    previous = map->prevs_buffer;
    i = 0;

    while (i <= map->last_level && previous[i]->next[i] == key_node)
    {
        previous[i]->next[i] = key_node->next[i];
        ++i;
    }
    
    if (key_node->next[0] != NULL)
    	key_node->next[0]->prev = previous[0];
    
    removed_value = key_node->value;
    
    fsmpool_free(&map->nodes_pools[i - 1], key_node);
    
    i = map->last_level;
    
    while (i > 0 && previous[i] == map->head && previous[i]->next[i] == NULL)
    	--i;
    
    map->last_level = i;

    map->size--;

    return removed_value;
}

size_t ulsmap_count(ULSkipMap const* map)
{
	assert(map != NULL);
	return map->size;
}

void ulsmap_clear(ULSkipMap* map)
{
	int i;

	assert(map != NULL);

    map->size = 0;
    map->last_level = 0;
	
	clear_mempools(map);

    init_head(map);
    
    for (i = map->max_level - 1; i >= 0; --i)
    {
    	map->prevs_buffer[i] = map->head;
    	map->tail_nodes[i] = map->head;
    }
}

void ulsmap_free(ULSkipMap* map)
{
	assert(map != NULL);
	
    clear_mempools(map);
    free(map->nodes_pools);
    free(map->tail_nodes);
    free(map->prevs_buffer);
}
