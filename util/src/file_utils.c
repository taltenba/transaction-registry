/**
 * \file
 * \brief 	Utility routines related to files
 * \date    Created:  25/03/2017
 * \date    Modified: 25/03/2017
 * \author  ALTENBACH Thomas (thomas.altenbach@utbm.fr)
 */

#include <stdio.h>
#include <assert.h>
#include <file_utils.h>
#include <string_utils.h>

BOOL file_read_bool(FILE* file, char* buffer, size_t buffer_size, BOOL* result)
{
	assert(file != NULL);
	assert(buffer != NULL);
	assert(result != NULL);
	
	return fgets(buffer, buffer_size, file) != NULL && parse_bool(buffer, result);
}

BOOL file_read_int(FILE* file, char* buffer, size_t buffer_size, int* result)
{
	assert(file != NULL);
	assert(buffer != NULL);
	assert(result != NULL);
	
	return fgets(buffer, buffer_size, file) != NULL && parse_int(buffer, result);
}

BOOL file_read_long(FILE* file, char* buffer, size_t buffer_size, long* result)
{
	assert(file != NULL);
	assert(buffer != NULL);
	assert(result != NULL);
	
	return fgets(buffer, buffer_size, file) != NULL && parse_long(buffer, result);
}

BOOL file_read_uint(FILE* file, char* buffer, size_t buffer_size, unsigned int* result)
{
	assert(file != NULL);
	assert(buffer != NULL);
	assert(result != NULL);
	
	return fgets(buffer, buffer_size, file) != NULL && parse_uint(buffer, result);
}

BOOL file_read_ulong(FILE* file, char* buffer, size_t buffer_size, unsigned long* result)
{
	assert(file != NULL);
	assert(buffer != NULL);
	assert(result != NULL);
	
	return fgets(buffer, buffer_size, file) != NULL && parse_ulong(buffer, result);
}

BOOL file_read_float(FILE* file, char* buffer, size_t buffer_size, float* result)
{
	assert(file != NULL);
	assert(buffer != NULL);
	assert(result != NULL);
	
	return fgets(buffer, buffer_size, file) != NULL && parse_float(buffer, result);
}

BOOL file_read_double(FILE* file, char* buffer, size_t buffer_size, double* result)
{
	assert(file != NULL);
	assert(buffer != NULL);
	assert(result != NULL);
	
	return fgets(buffer, buffer_size, file) != NULL && parse_double(buffer, result);
}

BOOL file_read_date_time(FILE* file, char* buffer, size_t buffer_size, time_t* result)
{
	assert(file != NULL);
	assert(buffer != NULL);
	assert(result != NULL);
	
	return fgets(buffer, buffer_size, file) != NULL && parse_date_time(buffer, result);
}
