/**
 * \file
 * \brief   Utility routines related to the generation of random numbers
 * \date    Created:  20/05/2017
 * \date    Modified: 12/12/2017
 * \author  ALTENBACH Thomas (thomas.altenbach@utbm.fr)
 */

#include <stdlib.h>
#include <assert.h>
#include <rnd_utils.h>

/**
 * \brief Minimum number of random bits in a random number generated thanks to the <rand> function 
 * 		  of the C standard library.
 */ 
#define BITS_PER_RAND 15

/**
 * \brief Mask of the the modulus m parameter of the linear congruential generator such that
 *        LCG_M_MASK = m - 1
 *
 * (LCG_M + 1) must be a power of two.
 */
#define LCG_M_MASK 4294967295UL

/**
 * \brief Multiplier parameter of the linear congruential generator
 *
 * LCG_A - 1 must be divisible by all prime factors of (LCG_M + 1).
 * LCG_A - 1 must be divisible by 4 if (LCG_M + 1) is divisible by 4.
 */
#define LCG_A 3950249UL

/**
 * \brief Increment parameter of the linear congruential generator
 *
 * LCG_C must be coprime with (LCG_M + 1).
 */
#define LCG_C 1UL

unsigned long rnd_between(unsigned long min, unsigned long max)
{
	assert(max > min);
	return rand() % (max - min) + min;
}

int rnd_pow2(unsigned int max_exp)
{
	static int rnd_bits = 0;
    static int remaining_bits = 0;
    
    int rnd_value;
    
    assert(max_exp > 0);
    assert(max_exp <= BITS_PER_RAND);

    if (remaining_bits < max_exp)
    {
        rnd_bits = rand();
        remaining_bits = BITS_PER_RAND;
    }

    rnd_value = rnd_bits & ((1U << max_exp) - 1);

    rnd_bits >>= max_exp;
    remaining_bits -= max_exp;

    return rnd_value;
}

unsigned long rnd_next_unique(unsigned long prev)
{
	return (LCG_A * prev + LCG_C) & LCG_M_MASK;
}
