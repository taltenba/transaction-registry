/**
 * \file
 * \brief 	Utility routines related to mathematical calculations
 * \date    Created:  12/05/2017
 * \date    Modified: 12/05/2017
 * \author  ALTENBACH Thomas (thomas.altenbach@utbm.fr)
 */

#include <math_utils.h>
#include <assert.h>

unsigned int ilog_floor(unsigned long value, unsigned int base_exp)
{
	unsigned int result;
	
    assert(value != 0);
    assert(base_exp != 0);

    result = 0;

    while (value >>= base_exp)
        ++result;

    return result;
}

unsigned int ilog_ceil(unsigned long value, unsigned int base_exp)
{
	assert(value != 0);
	assert(base_exp	!= 0);
	
	if (value == 1)
		return 0;
	
    return ilog_floor(value - 1, base_exp) + 1;
}
