/**
 * \file
 * \brief 	Generic map matching IDs (positive integer keys) with values
 * \date 	Created:  10/05/2017
 * \date	Modified: 18/11/2017
 * \author 	ALTENBACH Thomas (thomas.altenbach@utbm.fr)
 *
 * Behave in a similar way to an hash map with positive integer keys : entries are stored in an array 
 * and indexed according to the value of their key. The array is dynamically resized when the load  
 * factor exceed 0.75 so as to enable to store new elements without affecting too much performance.
 * Collisions are resolved using open addressing and linear probing.
 */
 
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <id_map.h>
 
#define MIN_INIT_CAPACITY 32		/**< Minimum capacity of a new IDMap (must be a power of two) */
#define DEFAULT_INIT_CAPACITY 32	/**< Default capacity of a new IDMap (must be a power of two) */
 
/**
 * \struct Entry
 * \brief An entry of an IDMap
 */
typedef struct idmap_entry
{
	unsigned long id;	/**< ID of the entry */
	void* value;		/**< Value of the entry */
} Entry;

/**
 * \brief Returns the minimum capacity of a map given the number of entries 
 * 	      so as to guarantee a maximum load factor of 0.75
 * \param[in] entries_count Number of entries
 * \return The minimum capacity of the map
 */
static size_t min_capacity(size_t entries_count)
{
	return entries_count + entries_count / 3;
}

/**
 * \brief Returns the smallest valid capacity of a map able to store the given number of elements
 * \param[in] entries_count Number of entries
 * \return The smallest valid capacity
 *
 * A number is considered as a valid capacity iff this number:
 *	- Is greater or equals than <min_capacity>(entries_count) to guarantee a maximum load factor of 0.75
 *	- Is a power of two to enable some calculation optimizations
 */
static size_t to_valid_capacity(size_t entries_count)
{
	size_t capacity = MIN_INIT_CAPACITY;
	size_t min_cap = min_capacity(entries_count);
	
	while (capacity < min_cap)
		capacity <<= 1;
	
	return capacity;
}

/**
 * \brief Indicates whether or not an entry is empty (ie. not used)
 * \param[in] entry The entry
 * \return TRUE if the entry is empty, FALSE otherwise
 */
static BOOL is_empty(Entry const* entry)
{
	return entry->value == NULL;
}

/**
 * \brief Returns the index of the slot in the map corresponding to the given ID
 * \param[in] map The map
 * \param[in] id The id
 * \return The index of the slot in the map corresponding to the given ID
 */
static size_t get_index(IDMap const* map, unsigned long id)
{
	return id & (map->capacity - 1);
}

/**
 * \brief Finds the entry with the specified ID in a IDMap (or an empty slot for a new entry 
 * 		  with this ID if the latter doesn't already exist in the map and find_empty is TRUE)
 * \param[in] map The map
 * \param[in] id The ID
 * \param[in] find_empty TRUE if empty slots are regarded as valid for the search, FALSE otherwise
 * \return The entry with the specified ID (or an empty slot for a new entry 
 * 		  with this ID if the latter doesn't already exist in the map and 
 *		  find_empty is TRUE) on success, NULL otherwise (no valid slot has been found)
 *
 * Firstly checks the slot corresponding to the given ID and if it is not valid, then checks 
 * all the slots in the array, starting from the firstly-checked one until finding a valid slot
 * or having checked all slots in the array.
 */
static Entry* find_entry(IDMap const* map, unsigned long id, BOOL find_empty)
{
	size_t i;
	size_t id_index = get_index(map, id);
	
	if (is_empty(&map->table[id_index]))
	{
		if (find_empty)
			return &map->table[id_index];
		else
			return NULL;
	}
	
	if (map->table[id_index].id == id)
		return &map->table[id_index];
	
	i = get_index(map, id_index + 1);
	
	while (!is_empty(&map->table[i]) && map->table[i].id != id && i != id_index)
	{
		i = get_index(map, i + 1);
	}
	
	if (i == id_index || (!find_empty && is_empty(&map->table[i])))
		return NULL;
	
	return &map->table[i];
}

/**
 * \brief Doubles the capacity of an IDMap
 * \param[in] map The map
 * \return TRUE on success, FALSE otherwise (out of memory)
 */
static BOOL resize_map(IDMap* map)
{
	size_t new_capacity;
	Entry* old_entry;
	Entry* new_entry;
	Entry* old_table_end;
	Entry* old_table;
	Entry* new_table;
	
	new_capacity = map->capacity << 1;
	new_table = calloc(new_capacity, sizeof(Entry));
	
	if (new_table == NULL)
		return FALSE;
	
	old_table = map->table;
	old_table_end = &old_table[map->capacity];
	map->table = new_table;
	map->capacity = new_capacity;
	
	for (old_entry = old_table; old_entry < old_table_end; ++old_entry)
	{
		if (!is_empty(old_entry))
		{
			new_entry = find_entry(map, old_entry->id, TRUE);
			new_entry->id = old_entry->id;
			new_entry->value = old_entry->value;
		}
	}
	
	free(old_table);
	return TRUE;
}

BOOL idmap_init(IDMap* map, size_t init_capacity)
{
	assert(map != NULL);
	
	if (init_capacity == 0)
		init_capacity = DEFAULT_INIT_CAPACITY;
	else
		init_capacity = to_valid_capacity(init_capacity);
	
	map->table = calloc(init_capacity, sizeof(Entry));
	
	if (map->table == NULL)
		return FALSE;
	
	map->capacity = init_capacity;
	map->entries_count = 0;
	
	return TRUE;
}

BOOL idmap_put(IDMap* map, unsigned long id, void* value)
{
	Entry* entry;
	
	assert(map != NULL);
	assert(value != NULL);
	
	if (map->capacity < min_capacity(map->entries_count + 1))
		resize_map(map);
	
	entry = find_entry(map, id, TRUE);
	
	if (entry == NULL)	/* Out of memory */
		return FALSE;
	
	if (is_empty(entry))
	{
		map->entries_count++;
		entry->id = id;
	}
	
	entry->value = value;
	
	return TRUE;
}

void* idmap_remove(IDMap* map, unsigned long id)
{
	void* removed_value;
	unsigned long last_removed_index;
	Entry* last_removed_entry;
	size_t i;
	
	assert(map != NULL);
	
	last_removed_entry = find_entry(map, id, FALSE);
	
	if (last_removed_entry == NULL)
		return NULL;
	
	removed_value = last_removed_entry->value;
	last_removed_index = last_removed_entry - map->table;
	i = last_removed_index + 1;
	
	while (i < map->capacity && !is_empty(&map->table[i]))
	{
		if (get_index(map, map->table[i].id) <= last_removed_index)
		{
			memcpy(&map->table[last_removed_index], &map->table[i], sizeof(Entry));
			last_removed_index = i;
			last_removed_entry = &map->table[i];
		}
		
		++i;
	}
	
	last_removed_entry->id = 0;
	last_removed_entry->value = NULL;
	
	map->entries_count--;
	
	return removed_value;
}

void* idmap_get(IDMap const* map, unsigned long id)
{
	Entry* entry;
	
	assert(map != NULL);
	
	entry = find_entry(map, id, FALSE);
	
	if (entry == NULL)
		return NULL;
	
	return entry->value;
}

void idmap_it_init(IDMap* map, IDMapIterator* it)
{
	assert(map != NULL);
	assert(it != NULL);
	
	it->map = map;
	it->next = map->table;
}

BOOL idmap_it_has_next(IDMapIterator* it)
{
	Entry* next;
	Entry* table_end;
	
	assert(it != NULL);
	
	table_end = &it->map->table[it->map->capacity];
	next = it->next;
	
	while (next < table_end && is_empty(next))
		++next;
		
	it->next = next;
	
	return next < table_end;
}

BOOL idmap_it_has_prev(IDMapIterator* it)
{
	Entry* prev;
	Entry* table_start;
	
	assert(it != NULL);
	
	table_start = &it->map->table[0];
	prev = it->next - 1;
	
	while (prev > table_start && is_empty(prev))
		--prev;
		
	if (prev < table_start || is_empty(prev))
		return FALSE;
		
	it->next = prev + 1;
	
	return TRUE;
}

void* idmap_it_next(IDMapIterator* it)
{
	Entry* next;
	
	assert(it != NULL);
	
	if (!idmap_it_has_next(it))
		return NULL;
		
	next = it->next;
	it->next++;
	
	return next->value;
}

void* idmap_it_prev(IDMapIterator* it)
{
	Entry* prev;
	
	assert(it != NULL);
	
	if (!idmap_it_has_prev(it))
		return NULL;
	
	prev = it->next - 1;
	it->next--;
	
	return prev->value;
}

size_t idmap_count(IDMap const* map)
{
	assert(map != NULL);
	return map->entries_count;
}

void idmap_free(IDMap* map)
{
	assert(map != NULL);
	
	free(map->table);
	memset(map, 0, sizeof(IDMap));
}
