/**
 * \file
 * \brief 	Utility routines related to files
 * \date    Created:  25/03/2017
 * \date    Modified: 25/03/2017
 * \author  ALTENBACH Thomas (thomas.altenbach@utbm.fr)
 */

#ifndef FILE_UTILS_H_INCLUDED
#define FILE_UTILS_H_INCLUDED

#include <bool.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

/**
 * \brief Reads one line of a file and converts the read string to boolean
 * \param[in] file The file
 * \param[in] buffer The buffer used to read the line
 * \param[in] buffer_size The size of the buffer
 * \param[out] result The read boolean on success
 * \return TRUE on success, FALSE otherwise
 *
 * The operation succeed iff :
 *     - At least one character has been read before end of line or file
 *     - The read string can be converted to a boolean value (as defined in <parse_bool>
 *       documentation)
 *
 * On failure, nothing is written into result.
 */
BOOL file_read_bool(FILE* file, char* buffer, size_t buffer_size, BOOL* result);

/**
 * \brief Reads one line of a file and converts the read string to int
 * \param[in] file The file
 * \param[in] buffer The buffer used to read the line
 * \param[in] buffer_size The size of the buffer
 * \param[out] result The read int on success
 * \return TRUE on success, FALSE otherwise
 *
 * The operation succeed iff :
 *     - At least one character has been read before end of line or file
 *     - The read string can be converted to an int value (as defined in <parse_int>
 *       documentation)
 *
 * On failure, nothing is written into result.
 */
BOOL file_read_int(FILE* file, char* buffer, size_t buffer_size, int* result);

/**
 * \brief Reads one line of a file and converts the read string to long
 * \param[in] file The file
 * \param[in] buffer The buffer used to read the line
 * \param[in] buffer_size The size of the buffer
 * \param[out] result The read long on success
 * \return TRUE on success, FALSE otherwise
 *
 * The operation succeed iff :
 *     - At least one character has been read before end of line or file
 *     - The read string can be converted to an long value (as defined in <parse_long>
 *       documentation)
 *
 * On failure, nothing is written into result.
 */
BOOL file_read_long(FILE* file, char* buffer, size_t buffer_size, long* result);

/**
 * \brief Reads one line of a file and converts the read string to unsigned int
 * \param[in] file The file
 * \param[in] buffer The buffer used to read the line
 * \param[in] buffer_size The size of the buffer
 * \param[out] result The read unsigned int on success
 * \return TRUE on success, FALSE otherwise
 *
 * The operation succeed iff :
 *     - At least one character has been read before end of line or file
 *     - The read string can be converted to an unsigned int value (as defined in <parse_uint>
 *       documentation)
 *
 * On failure, nothing is written into result.
 */
BOOL file_read_uint(FILE* file, char* buffer, size_t buffer_size, unsigned int* result);

/**
 * \brief Reads one line of a file and converts the read string to unsigned long
 * \param[in] file The file
 * \param[in] buffer The buffer used to read the line
 * \param[in] buffer_size The size of the buffer
 * \param[out] result The read unsigned long on success
 * \return TRUE on success, FALSE otherwise
 *
 * The operation succeed iff :
 *     - At least one character has been read before end of line or file
 *     - The read string can be converted to an unsigned long value (as defined in <parse_ulong>
 *       documentation)
 *
 * On failure, nothing is written into result.
 */
BOOL file_read_ulong(FILE* file, char* buffer, size_t buffer_size, unsigned long* result);

/**
 * \brief Reads one line of a file and converts the read string to float
 * \param[in] file The file
 * \param[in] buffer The buffer used to read the line
 * \param[in] buffer_size The size of the buffer
 * \param[out] result The read float on success
 * \return TRUE on success, FALSE otherwise
 *
 * The operation succeed iff :
 *     - At least one character has been read before end of line or file
 *     - The read string can be converted to a float value (as defined in <parse_float>
 *       documentation)
 *
 * On failure, nothing is written into result.
 */
BOOL file_read_float(FILE* file, char* buffer, size_t buffer_size, float* result);

/**
 * \brief Reads one line of a file and converts the read string to double
 * \param[in] file The file
 * \param[in] buffer The buffer used to read the line
 * \param[in] buffer_size The size of the buffer
 * \param[out] result The read double on success
 * \return TRUE on success, FALSE otherwise
 *
 * The operation succeed iff :
 *     - At least one character has been read before end of line or file
 *     - The read string can be converted to a double value (as defined in <parse_double>
 *       documentation)
 *
 * On failure, nothing is written into result.
 */
BOOL file_read_double(FILE* file, char* buffer, size_t buffer_size, double* result);

/**
 * \brief Reads one line of a file and converts the read string to a date time
 * \param[in] file The file
 * \param[in] buffer The buffer used to read the line
 * \param[in] buffer_size The size of the buffer
 * \param[out] result The read date time on success
 * \return TRUE on success, FALSE otherwise
 *
 * The operation succeed iff :
 *     - At least one character has been read before end of line or file
 *     - The read string can be converted to a date (as defined in <parse_date>
 *       documentation)
 *
 * On failure, nothing is written into result.
 */
BOOL file_read_date_time(FILE* file, char* buffer, size_t buffer_size, time_t* result);

#endif /* FILE_UTILS_H_INCLUDED */
