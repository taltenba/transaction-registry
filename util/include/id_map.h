/**
 * \file
 * \brief 	Generic map matching IDs (positive integer keys) with values
 * \date 	Created:  10/05/2017
 * \date	Modified: 18/11/2017
 * \author 	ALTENBACH Thomas (thomas.altenbach@utbm.fr)
 *
 * Behaves in a similar way to an hash map with positive integer keys : entries are stored in an 
 * array and indexed according to the value of their key. The array is dynamically resized when the 
 * load factor exceed 0.75 to be able store new elements without affecting too much performance.
 * Collisions are resolved using open addressing and linear probing.
 */

#ifndef ID_MAP_H
#define ID_MAP_H

#include <bool.h>
#include <stdlib.h>

struct idmap_entry;

/**
 * \struct IDMap
 * \brief Generic map matching IDs (positive integer keys) with values
 */
typedef struct
{
	struct idmap_entry* table;	/**< Array containing the entries of the map */
	size_t capacity;			/**< Size of the table array (must be a power of two) */
	size_t entries_count;		/**< Number of entries stored in the map */
} IDMap;

typedef struct
{
	IDMap* map;                 /**< The map */
	struct idmap_entry* next;   /**< The next element in the iteration */
} IDMapIterator;

/**
 * \brief Initializes a new IDMap
 * \param[in] map The map
 * \param[in] init_capacity The initial capacity of the map
 * \return TRUE on success, FALSE on failure (out of memory)
 *
 * The init_capacity argument is optional and may be set to the max number
 * of entries expected to be put in the map. This is used as a hint to pre-allocate
 * the needed memory and to avoid unecessary rehashes. If init_capacity is 0, the
 * default size will be used.
 *
 * When no more in use, the initialized map must be freed with <idmap_free>.
 */
BOOL idmap_init(IDMap* map, size_t init_capacity);

/**
 * \brief Puts a new element in an IDMap
 * \param[in] map The map
 * \param[in] id Id of the new element
 * \param[in] value Value of the new element (cannot be NULL)
 * \return TRUE on success, FALSE otherwise (out of memory)
 * 
 * If the map previously contained a mapping for the id, 
 * the old value is replaced by the new one.
 */
BOOL idmap_put(IDMap* map, unsigned long id, void* value);

/**
 * \brief Removes an entry from an IDMap
 * \param[in] map The map
 * \param[in] id Id of the entry to remove
 * \return The value of the removed entry if the map contained a mapping
 * 		   for the id, NULL otherwise
 */
void* idmap_remove(IDMap* map, unsigned long id);

/**
 * \brief Gets the value associated with the specified ID in an IDMap
 * \param[in] map The map
 * \param[in] id The id
 * \return The value associated with the specified ID in the map if the latter
 *		   contains a mapping for that ID, NULL otherwise
 */
void* idmap_get(IDMap const* map, unsigned long id);

/**
 * \brief Initializes an iterator over the elements of an IDMap
 * \param[in] map The map
 * \param[out] it The iterator to initialize
 */
void idmap_it_init(IDMap* map, IDMapIterator* it);

/**
 * \brief Indicates whether or not there is a next element in an iteration
 * \param[in] it The iterator
 * \return TRUE if there is a next element to return, FALSE otherwise
 */
BOOL idmap_it_has_next(IDMapIterator* it);

/**
 * \brief Indicates whether or not there is a previous element in an iteration
 * \param[in] it The iterator
 * \return TRUE if there is a previous element to return, FALSE otherwise
 */
BOOL idmap_it_has_prev(IDMapIterator* it);

/**
 * \brief Gets the next element in the iteration
 * \param[in] it The iterator
 * \return The next element in the iteration if there is one, NULL otherwise
 *
 * The values are returned in no particular order.
 * Note that alternating calls to <idmap_it_next> and <idmap_it_previous> will return the same 
 * element repeatedly.
 */
void* idmap_it_next(IDMapIterator* it);

/**
 * \brief Gets the previous element in the iteration
 * \param[in] it The iterator
 * \return The previous element in the iteration if there is one, NULL otherwise
 *
 * The values are returned in no particular order.
 * Note that alternating calls to <idmap_it_next> and <idmap_it_previous> will return the same 
 * element repeatedly.
 */
void* idmap_it_prev(IDMapIterator* it);

/**
 * \brief Returns the number of elements stored in an IDMap
 * \param[in] map The map
 * \return The number of elements stored in the map
 */
size_t idmap_count(IDMap const* map);

/**
 * \brief Free all the memory associated with an IDMap
 * \param[in] map The map
 */
void idmap_free(IDMap* map);

#endif /* ID_MAP_H */
