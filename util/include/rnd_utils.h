/**
 * \file
 * \brief   Utility routines related to the generation of random numbers
 * \date    Created:  20/05/2017
 * \date    Modified: 12/12/2017
 * \author  ALTENBACH Thomas (thomas.altenbach@utbm.fr)
 */

#ifndef RND_UTILS_H_INCLUDED
#define RND_UTILS_H_INCLUDED


/**
 * \brief Generates a random integer between two numbers
 * \param[in] min Lower bound (inclusive)
 * \param[in] max Upper bound (exclusive, max > min)
 * \return A random integer between min and max
 */
unsigned long rnd_between(unsigned long min, unsigned long max);

/**
 * \brief Generates a random integer between 0 (inclusive) and a specified power of two (exlusive)
 * \param[in] max_exp Exponent of the power of two acting as the upper limit (has to fall within 
 *					  the interval [1,15])
 * \return A random integer between 0 (inclusive) and the specified power of two (exlusive)
 */
int rnd_pow2(unsigned int exponent);

/**
 * \brief Generates the next integer of sequence of 2^32 unique random number between 0 (inclusive) 
 *        and 2^32 (exclusive)
 * \param[in] prev The previous random integer of the sequence (or the seed of the sequence if 
 *                 no previous number has been generated).
 * \return A unique random number between 0 (inclusive) and 2^32 (exlusive).
 */
unsigned long rnd_next_unique(unsigned long prev);

#endif /* RND_UTILS_H_INCLUDED */
