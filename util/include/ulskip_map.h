/**
 * \file
 * \brief   Sorted generic map matching postive integer keys with values using a skip-list
 * \date    Created: 11/05/2017
 * \date    Modified: 26/12/2017
 * \author  ALTENBACH Thomas (thomas.altenbach@utbm.fr)
 *
 * Entries are stored in ascending order of the keys, enabling that way sorted traversal of the map.
 *
 * Implemented over a skip-list, this data structure guarantee therefore on average similar
 * performance as a self-balancing tree-based map, that is to say logarithmic complexity for searches,
 * insertions, and removals. 
 * 
 * Storing on average only 7/3 pointers per nodes, in addition to the data, this data structure also 
 * use less memory than a typical tree-map implementation.
 *
 * Moreover, insertions and removals in a skip-list are often less expensive than in a self-balancing 
 * tree since neither traversal nor rebalancing is needed.
 * In particular, when those operations are performed in ascending or descending order, they are
 * completed in constant time by the skip-list.
 *
 * However, by the probabilistic nature of the skip-list, performance is less stable than in a 
 * tree-map and the logarithmic complexity of the searches, insertions and removals is only 
 * guaranteed on average, although it is extremely unlikely that the real performance moves far away 
 * from the average one (and more the skip-list contains elements, more the likelihood decreases).
 */

#ifndef ULSKIP_MAP_H_INCLUDED
#define ULSKIP_MAP_H_INCLUDED

#include <bool.h>
#include <fsmem_pool.h>

/**
 * \struct ULSkipNode
 * \brief Node of a ULSkipMap
 */
typedef struct ulskipnode ULSkipNode;

/**
 * \struct ULSkipMap
 * \brief Skip-map with integer keys
 */
typedef struct
{
    ULSkipNode* head;            /**< Header node of the map */
    ULSkipNode** tail_nodes;     /**< Array of last nodes of the map on all levels */
    ULSkipNode** prevs_buffer;   /**< Buffer used to temporarily store the previous nodes of a node, avoiding to dynamically allocate then free a buffer each time the latter is required */
    size_t size;                 /**< Number of entries in the map */
    int last_level;              /**< Index of the current last layer of the list */
    int max_level;               /**< Current maximum number of levels possible in the map */
    FSMemPool* nodes_pools;      /**< Array indexed by level of memory pools used for allocating the nodes of the map */
} ULSkipMap;

/**
 * \struct ULSkipMapIterator
 * \brief Iterator allowing to traverse an ULSkipMap in the ascending order of the keys
 */
typedef struct
{
	ULSkipNode* cur;             /**< Current node in the iteration */
	unsigned long start_key;     /**< Start key value (inclusive) */
	unsigned long end_key;       /**< End key value (inclusive) */
} ULSkipMapIterator;

/**
 * \brief Initialize a new ULSkipMap
 * \param[out] map The map to initialize
 * \param[in] init_size Initial capacity of the map
 * \return TRUE on success, FALSE otherwise (out of memory)
 *
 * The init_capacity argument is optional and may be set to the max number
 * of entries expected to be put in the map. This is used as a hint to pre-allocate
 * the needed memory. If init_capacity is 0, the default size will be used.
 *
 * When no longer used, the initialized map must be freed using <ulsmap_free>
 */
BOOL ulsmap_init(ULSkipMap* map, size_t init_size);

/**
 * \brief Puts a new element in a ULSkipMap
 * \param[in] map The map
 * \param[in] key Key of the element
 * \param[in] value Value of the element (cannot be NULL)
 * \return TRUE on success, FALSE otherwise (out of memory)
 *
 * If the map previously contained a mapping for the key, 
 * the old value is replaced by the new one.
 */
BOOL ulsmap_put(ULSkipMap* map, unsigned long key, void* value);

/**
 * \brief Removes an entry from a ULSkipMap
 * \param[in] map The map
 * \param[in] key The key of the entry to remove
 * \return The value of the removed entry if the map contained a mapping for the given id, NULL otherwise
 */
void* ulsmap_remove(ULSkipMap* map, unsigned long key);

/**
 * \brief Gets the value associated with the specified key in a ULSkipMap
 * \param[in] map The map
 * \param[in] key The key
 * \return The value associated with the specified key in the map if the latter
 *         contains a mapping for that key, NULL otherwise
 */
void* ulsmap_get_value(ULSkipMap const* map, unsigned long key);

/**
 * \brief Initializes an iterator traversing all the elements having a key greater than the 
 *        specified value
 * \param[in] map The map
 * \param[in] min The minimum value (inclusive)
 * \param[out] it The iterator to intialize
 *
 * The map cannot be modified during the iteration.
 */
void ulsmap_get_from(ULSkipMap const* map, unsigned long min, ULSkipMapIterator* it);

/**
 * \brief Initializes an iterator traversing all the elements having a key grether than the 
 *        specified value
 * \param[in] map The map
 * \param[in] max The maximum value (inclusive)
 * \param[out] it The iterator to intialize
 *
 * The map cannot be modified during the iteration.
 */
void ulsmap_get_to(ULSkipMap const* map, unsigned long max, ULSkipMapIterator* it);

/**
 * \brief Initializes an iterator traversing all the elements of a ULSkipMap having a key 
 *        between the specified values
 * \param[in] map The map
 * \param[in] min The minimum value (inclusive)
 * \param[in] max The maximum value (inclusive, max >= min)
 * \param[out] it The iterator to intialize
 *
 * The map cannot be modified during the iteration.
 */
void ulsmap_get_between(ULSkipMap const* map, unsigned long min, unsigned long max, ULSkipMapIterator* it);

/**
 * \brief Initializes an iterator traversing all the elements of ULSkipMap having a key between the   
 *        specified values and starting from the end.
 * \param[in] map The map
 * \param[in] min The minimum value (inclusive)
 * \param[in] max The maximum value (inclusive, max >= min)
 * \param[out] it The iterator to intialize
 *
 * The map cannot be modified during the iteration.
 */
void ulsmap_get_between_reverse(ULSkipMap const* map, unsigned long min, unsigned long max, ULSkipMapIterator* it);

/**
 * \brief Initializes an iterator over the elements of a ULSkipMap
 * \param[in] map The map
 * \param[out] it The iterator to initialize
 *
 * The map cannot be modified during the iteration.
 */
void ulsmap_it_init(ULSkipMap const* map, ULSkipMapIterator* it);

/**
 * \brief Indicates whether or not there is a next element in an iteration
 * \param[in] it The iterator
 * \return TRUE if there is a next element to return, FALSE otherwise
 */
BOOL ulsmap_it_has_next(ULSkipMapIterator const* it);

/**
 * \brief Indicates whether or not there is a previous element in an iteration
 * \param[in] it The iterator
 * \return TRUE if there is a previous element to return, FALSE otherwise
 */
BOOL ulsmap_it_has_prev(ULSkipMapIterator const* it);

/**
 * \brief Gets the next element in the iteration
 * \param[in] it The iterator, having a next element
 * \return The next element in the iteration
 *
 * The values are returned in ascending order of keys.
 * The iterator must have a next element.
 * Note that alternating calls to <ulsmap_it_next> and <ulsmap_it_previous> will return the same 
 * element repeatedly.
 */
void* ulsmap_it_next(ULSkipMapIterator* it);

/**
 * \brief Gets the previous element in the iteration
 * \param[in] it The iterator, having a previous element
 * \return The previous element in the iteration
 *
 * The values are returned in descending order of keys.
 * The iterator must have a previous element.
 * Note that alternating calls to <ulsmap_it_next> and <ulsmap_it_previous> will return the same 
 * element repeatedly.
 */
void* ulsmap_it_prev(ULSkipMapIterator* it);

/**
 * \brief Returns the number of elements stored in a ULSkipMap
 * \param[in] map The map
 * \return The number of elements stored in the map
 */
size_t ulsmap_count(ULSkipMap const* map);

/**
 * \brief Removes all the elements of a ULSkipMap
 * \param[in] map The map
 */
void ulsmap_clear(ULSkipMap* map);

/**
 * \brief Free all the memory associated with a ULSkipMap
 * \param[in] map The map
 */
void ulsmap_free(ULSkipMap* slist);

#endif /* ULSKIP_MAP_H_INCLUDED */
