/**
 * \file
 * \brief   Boolean type
 * \date    Created:  07/03/2017
 * \date    Modified: 07/03/2017
 * \author  ALTENBACH Thomas (thomas.altenbach@utbm.fr)
 */

#ifndef BOOL_H
#define BOOL_H

/**
 * \enum BOOL
 * \brief The boolean type
 */
typedef enum {
	FALSE = 0,
	TRUE = 1
} BOOL;

#endif /* BOOL_H */
