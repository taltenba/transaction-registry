/**
 * \file
 * \brief 	Utility routines related to mathematical calculations
 * \date    Created:  12/05/2017
 * \date    Modified: 12/05/2017
 * \author  ALTENBACH Thomas (thomas.altenbach@utbm.fr)
 */

#ifndef MATH_UTILS_H_INCLUDED
#define MATH_UTILS_H_INCLUDED

/**
 * \brief Computes the integer part of the logarithm base a given power of two of the specified
 *        integer value
 * \param[in] value The value (> 0)
 * \param[in] base_exp The exponent of the power of two being the base of the logarithm (> 0)
 * \return The integer part of the logarithm base 2^(base_exp) of the value
 */
unsigned int ilog_floor(unsigned long value, unsigned int base_exp);

/**
 * \brief Computes the rounded up integer value of the logarithm base a given power of two of 
 *        the specified integer value
 * \param[in] value The value (> 0)
 * \param[in] base_exp The exponent of the power of two being the base of the logarithm (> 0)
 * \return The rounded up integer value of the logarithm base 2^(base_exp) of the value
 */
unsigned int ilog_ceil(unsigned long value, unsigned int base_exp);

#endif /* MATH_UTILS_H_INCLUDED */
