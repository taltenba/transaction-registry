## Set to 1 for debug, 0 for release
DEBUG ?= 1

## Selected compiler
CXX = gcc

## Compilation flags
ifeq ($(DEBUG), 1)
	export CFLAGS ?= -Wall -Werror -ansi -pedantic -g -O0
else
	export CFLAGS ?= -Wall -Werror -ansi -pedantic -O3 -DNDEBUG
endif

## Name of the directory where source .c files are stored
SRCDIR = src
## Name of the directory where object .o files are stored
OBJDIR = obj
## Name of the directory where header .h files are stored
INCLUDEDIR = include

# Registry library
## Library directory
LIBREGISTRY = registry
## Library file
LIBREGISTRY_TARGET = $(LIBREGISTRY)/lib$(LIBREGISTRY).so
## Library source .c files
LIBREGISTRY_CFILES = $(shell find $(LIBREGISTRY)/$(SRCDIR) -name '*.c')
## Library object .o files
LIBREGISTRY_OFILES = $(subst $(SRCDIR)/,$(OBJDIR)/, $(patsubst %.c,%.o, $(LIBREGISTRY_CFILES)))

# Utility library
## Library directory
LIBUTIL = util
## Library file
LIBUTIL_TARGET = $(LIBUTIL)/lib$(LIBUTIL).so
## Library source .c files
LIBUTIL_CFILES = $(shell find $(LIBUTIL)/$(SRCDIR) -name '*.c')
## Library object .o files
LIBUTIL_OFILES = $(subst $(SRCDIR)/,$(OBJDIR)/, $(patsubst %.c,%.o, $(LIBUTIL_CFILES)))

# Registry test application
## Application directory
APP = registry_test
## Application executable files
APP_TARGET = $(APP)/$(APP).exe
## Application source .c files
APP_CFILES = $(shell find $(APP)/$(SRCDIR) -name '*.c')
## Application object .o files
APP_OFILES = $(subst $(SRCDIR)/,$(OBJDIR)/, $(patsubst %.c,%.o, $(APP_CFILES)))

## Set of directories where object .o files of each library and application are stored
OBJDIRS = $(addsuffix /$(OBJDIR), $(LIBREGISTRY) $(LIBUTIL) $(APP))

## Directories of the libraries to include
LIBSDIRS = -L$(LIBUTIL) -L$(LIBREGISTRY) -L/usr/lib
## Names of the libraries to include
LIBS = -l$(LIBUTIL) -l$(LIBREGISTRY) -lcrypto -lncurses
## Directories of the header .h files to include
INCLUDEDIRS = -I$(LIBUTIL)/$(INCLUDEDIR) -I$(LIBREGISTRY)/$(INCLUDEDIR) -I$(APP)/$(INCLUDEDIR) -I/usr/include

all: $(APP_TARGET)	

run: $(APP_TARGET)
	export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:"$(addprefix $(CURDIR)/, $(LIBREGISTRY))":"$(addprefix $(CURDIR)/, $(LIBUTIL))"; \
	cd $(APP); \
	./$(APP).exe registry.dat

libregistry: $(LIBREGISTRY_TARGET)

libutil: $(LIBUTIL_TARGET)

$(APP_TARGET): $(APP)/$(OBJDIR) $(APP_OFILES) $(LIBREGISTRY_TARGET) $(LIBUTIL_TARGET)
	$(CXX) $(CFLAGS) $(APP_OFILES) $(LIBSDIRS) $(LIBS) -o $(APP_TARGET)

$(LIBREGISTRY_TARGET): $(LIBREGISTRY)/$(OBJDIR) $(LIBREGISTRY_OFILES)
	$(CXX) $(CFLAGS) -shared -fpic $(LIBREGISTRY_OFILES) -o $(LIBREGISTRY_TARGET)
	
$(LIBUTIL_TARGET): $(LIBUTIL)/$(OBJDIR) $(LIBUTIL_OFILES)
	$(CXX) $(CFLAGS) -shared -fpic $(LIBUTIL_OFILES) -o $(LIBUTIL_TARGET)

$(OBJDIRS):
	mkdir $@

.PHONY: clean

clean:
	rm -rf $(OBJDIRS) $(LIBREGISTRY_TARGET) $(LIBUTIL_TARGET) $(APP_TARGET)
	
.SECONDEXPANSION:

$(APP_OFILES): %.o: $$(subst $(OBJDIR)/,$(SRCDIR)/, %).c
	$(CXX) $(CFLAGS) $(INCLUDEDIRS) -c -o $@ $<


$(LIBREGISTRY_OFILES) $(LIBUTIL_OFILES): %.o: $$(subst $(OBJDIR)/,$(SRCDIR)/, %).c
	$(CXX) $(CFLAGS) $(INCLUDEDIRS) -fpic -c -o $@ $<
