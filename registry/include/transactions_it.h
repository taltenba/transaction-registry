/**
 * \file
 * \brief   Iterator able to traverse a list of transactions
 * \date    Created:  01/12/2017
 * \date    Modified: 01/12/2017
 * \author  ALTENBACH Thomas (thomas.altenbach@utbm.fr)
 */

#ifndef TRANSACTIONS_IT_H
#define TRANSACTIONS_IT_H

#include <transaction.h>
#include <ulskip_map.h>

/**
 * \brief Iterator able to traverse a list of transactions
 */
typedef ULSkipMapIterator TransactionsIterator;

/**
 * \brief Indicates whether or not there is a next element in an iteration
 * \param[in] it The iterator
 * \return TRUE if there is a next element to return, FALSE otherwise
 */
BOOL transactions_it_has_next(TransactionsIterator const* it);

/**
 * \brief Indicates whether or not there is a previous element in an iteration
 * \param[in] it The iterator
 * \return TRUE if there is a previous element to return, FALSE otherwise
 */
BOOL transactions_it_has_prev(TransactionsIterator const* it);

/**
 * \brief Gets the next transaction in the iteration
 * \param[in] it The iterator, having a next element
 * \return The next transaction in the iteration
 *
 * The iterator must have a next element.
 */
Transaction* transactions_it_next(TransactionsIterator* it);

/**
 * \brief Gets the previous transaction in the iteration
 * \param[in] it The iterator, having a previous element
 * \return The previous transaction in the iteration
 *
 * The iterator must have a previous element.
 */
Transaction* transactions_it_prev(TransactionsIterator* it);

#endif /* TRANSACTIONS_IT_H */

