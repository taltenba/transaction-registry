/**
 * \file
 * \brief   Transaction of the centralized transaction registry
 * \date    Created:  29/10/2017
 * \date    Modified: 03/01/2018
 * \author  ALTENBACH Thomas (thomas.altenbach@utbm.fr)
 * \author  GUERET Alexis (alexis.gueret@utbm.fr)
 */

#ifndef TRANSACTION_H
#define TRANSACTION_H

#include <bool.h>
#include <openssl/sha.h>

/**
 * \brief Maximum length of a string representing a transaction status
 */
#define MAX_TRANSACTION_STATUS_LENGTH 9

/**
 * \enum TransactionStatus
 * \brief Enumerates the possible status of a transaction
 *
 * All enum values are non-zero.
 */
typedef enum
{
    PENDING = 1,
    CONFIRMED = 2,
    EXECUTED = 4,
    CANCELED = 8
} TransactionStatus;

/**
 * \struct Transaction
 * \brief A transaction of the centralized transaction registry
 */
typedef struct
{
    unsigned long id;                                   /**< The public transaction identifier */
    unsigned long emitter_id;                           /**< The transaction emitter's identifier */
    unsigned long recipient_id;                         /**< The transaction recipient's identifier */
    unsigned long start_date_time;                      /**< The timestamp in seconds of the creation of the transaction */
    unsigned long end_date_time;                        /**< The timestamp in seconds of the end of the transaction, set after the transaction has been executed or canceled */
    unsigned long ttl;                                  /**< The time-to-live of the transaction in seconds */
    double amount;                                      /**< The amount of money of the transaction, positive non-zero value */
	TransactionStatus status;                           /**< The transaction current status */
    unsigned char private_key[SHA256_DIGEST_LENGTH];    /**< The private key used to secure the transaction. That array does *not* end with a terminating null character */
} Transaction;

/**
 * \brief Creates a new pending transaction with the specified ID, recipient ID, amount and 
 *        start_date_time, and a randomly generated private key.
 * \param[out] transaction The newly created transaction
 * \param[in] id The transaction unique identifier (> 0)
 * \param[in] recipient_id The recipient identifier
 * \param[in] amount The amount of the transaction (> 0.0)
 * \param[in] start_date_time The timestamp in seconds of the creation of the transaction
 * \return TRUE on success, FALSE otherwise (generation of the private key has failed)
 *
 * The time-to-live of the the transaction is set to the default value.
 *
 * This function assumes that the OpenSSL library random number generator have been 
 * formely initialized.
 */
BOOL transaction_create(Transaction* transaction, unsigned long id, unsigned long recipient_id, double amount, unsigned long start_date_time);

/**
 * \brief Gets the ID of the given transaction
 * \param[in] transaction The transaction
 * \return The ID of the transaction
 */
unsigned long transaction_get_id(Transaction const* transaction);

/**
 * \brief Gets the amount of the given transaction
 * \param[in] transaction The transaction
 * \return The amount of the transaction
 */
double transaction_get_amount(Transaction const* transaction);

/**
 * \brief Gets the current status of the given transaction
 * \param[in] transaction The transaction
 * \return The current status of the transaction
 */
TransactionStatus transaction_get_status(Transaction const* transaction);

/**
 * \brief Gets the recipient ID of the given transaction
 * \param[in] transaction The transaction
 * \return The recipient ID of the transaction
 */
unsigned long transaction_get_recipient_id(Transaction const* transaction);

/**
 * \brief Gets the emitter ID of a confirmed or executed transaction
 * \param[in] transaction The confirmed or executed transaction
 * \return The emitter ID of the transaction
 */
unsigned long transaction_get_emitter_id(Transaction const* transaction);

/**
 * \brief Gets the timestamp in seconds of the creation of the given transaction
 * \param[in] transaction The transaction
 * \return The timestamp in seconds of the creation of the transaction
 */
unsigned long transaction_get_start_date_time(Transaction const* transaction);

/**
 * \brief Gets the timestamp in seconds of the end of the given transaction
 * \param[in] transaction The transaction
 * \return The timestamp in seconds of the end of the transaction
 *
 * The transaction must be either executed or canceled.
 */
unsigned long transaction_get_end_date_time(Transaction const* transaction);

/**
 * \brief Gets the time-to-tive of the given transaction in seconds
 * \param[in] transaction The transaction
 * \return The time-to-live of the transaction in seconds
 */
unsigned long transaction_get_ttl(Transaction const* transaction);

/**
 * \brief Gets the private key of the given transaction
 * \param[in] transaction The transaction
 * \return The private key of the transaction
 */
unsigned char const* transaction_get_private_key(Transaction const* transaction);

/**
 * \brief Checks if the given key is the private key of the specified transaction
 * \param[in] transaction The transaction
 * \param[in] key The key (assumed to be a 32-bytes array)
 * \return TRUE if the key is the private key of the transaction, FALSE otherwise
 */
BOOL transaction_check_key(Transaction const* transaction, unsigned char const* key);

/**
 * \brief Indicates if the given transaction is expired
 * \param[in] transaction The transaction
 * \param[in] cur_date The current timestamp in seconds
 * \return TRUE if the transaction is expired, FALSE otherwise
 */
BOOL transaction_is_expired(Transaction const* transaction, unsigned long cur_date);

/**
 * \brief Serializes a transaction to a string
 * \param[in] transaction The transaction
 * \param[out] buffer The buffer into which the transaction must be serialized
 *
 * The buffer must be big enough to store the serialized transaction.
 */
void transaction_serialize(Transaction const* transaction, char* buffer);

/**
 * \brief Deserializes a transaction from a string
 * \param[in] str The string
 * \param[out] transaction The deserialized transaction
 * \return TRUE on success, FALSE otherwise (the string is not a valid serialized transaction)
 *
 * The input string is modified during the process.
 */
BOOL transaction_deserialize(char* str, Transaction* transaction);

/**
 * \brief Converts a transaction status into its corresponding string representation
 * \param[in] status The transaction status
 * \param[out] str The string representing the status (must be MAX_TRANSACTION_STATUS_LENGTH-
 *                 characters long, without taking the null terminating character into account).
 */
void print_transaction_status(TransactionStatus status, char* str);

/**
 * \brief Converts a transaction key into its corresponding hexadecimal string representation
 * \param[in] key The 32-bytes transaction key
 * \param[out] str The string in which to write the key (must be at least 65-characters long for
 *                 the 64 hexadecimal key characters and the null terminating character).
 */
void print_transaction_key(unsigned char const* key, char* str);

/**
 * \brief Converts an hexadecimal string into its corresponding transaction key
 * \param[in] key_str The hexadecimal key representation
 * \param[out] key The transaction key (32-bytes long)
 * \return TRUE on success, FALSE otherwise (the string is not representing a valid key)
 */
BOOL parse_transaction_key(char const* str, unsigned char* key);

#endif /* TRANSACTION_H */

