/**
 * \file
 * \brief   Centralized and secured transaction registry
 * \date    Created:  29/10/2017
 * \date    Modified: 03/01/2018
 * \author  ALTENBACH Thomas (thomas.altenbach@utbm.fr)
 *
 * Transaction registry keeping track of monetary exchanges between different actors while securing
 * the transactions.
 *
 * Each transaction is defined in particular by two users, one called recipient (the one who will
 * receive the money) and an emitter (the one who will send the money).
 *
 * The registry is intended to secure this transaction in that it ensures the emitter has a sufficient
 * balance to complete the transaction, preventing the emitter from giving an invalid payment solution
 * the recipient.
 *
 * To achieve this aim the transaction process follows three phases:
 *     - The recipient performs a transaction request in the registry, specifying the amount, and will be 
 *       given a transaction ID. The created transaction is then "PENDING".
 *     - The recipient gives that ID to the emitter and the latter has to confirmed the transaction
 *       before it expires. If the emitter has enough money the transaction amount is withdrawn from its
 *       balance. In that case the transaction will be "CONFIRMED" and a private transaction key will be
 *       given to the emitter. After the transaction has been confirmed it cannot be canceled anymore.
 *     - During the exchange, in order to pay the recipient and complete the transaction, the emitter 
 *       gives the key to the recipient. Having that key the latter will then be able to execute the 
 *       transaction and its balance will be credited with the transaction amount.
 *
 * Thus the registry allows the recipient to check instantaneously the validity of the payment solution
 * before to complete the exchange.
 *
 * This registry implementation can store at most (2^32 - 1) transactions and the same number of users.
 */

#ifndef REGISTRY_H
#define REGISTRY_H

#include <bool.h>
#include <user.h>
#include <transaction.h>
#include <fsmem_pool.h>
#include <id_map.h>
#include <ulskip_map.h>
#include <users_it.h>

/**
 * \struct TransactionElement
 * \brief Represents an element of the registry, holding a transaction
 */
typedef struct transacelem {
    Transaction transaction;        /**< The transaction held by the element */
    struct transacelem* prev;       /**< The previous element in the registry */
    struct transacelem* next;       /**< The next element in the registry */
} TransactionElement;

/**
 * \struct Registry
 * \brief Doubly linked list of transactions
 */
typedef struct
{
    TransactionElement* head;               /**< Pointer to the first element in the registry */
    TransactionElement* tail;               /**< Pointer to the last element in the registry */
    size_t transaction_count;               /**< The number of transactions held by the registry */
    unsigned long last_transaction_id;      /**< The last generated transaction id */
    unsigned long last_user_id;             /**< The last generated user id */
    unsigned long last_transaction_date;    /**< The last transaction date (timestamp in seconds) */
    IDMap transactions_ids_map;				/**< Map matching transactions with their id */
    IDMap users_ids_map;                    /**< Map matching users with their id */
    ULSkipMap transactions_dates_map;		/**< Sorted map matching transactions with their creation date */
    FSMemPool transactions_pool;            /**< Memory pool for the transactions elements of the registry */
    FSMemPool users_pool;                   /**< Memory pool for the users of the registry */
} Registry;

/**
 * \enum RegLoadResult
 * \brief Defines the possible output values of the function <registry_load>
 */ 
typedef enum
{
	REGLOAD_SUCCESS = 0,                     /**< Registry has been successfully loaded */
	REGLOAD_FILE_NOT_FOUND = -1,             /**< Error: registry file not found */
	REGLOAD_INVALID_FILE = -2,               /**< Error: the file is not a valid registry file */
	REGLOAD_OUT_OF_MEMORY = -3               /**< Error: out of memory */
} RegLoadResult;

/**
 * \brief Initializes a new registry with random content
 * \param[out] registry The registry to initialize
 * \return TRUE on success, FALSE otherwise (out of memory)
 *
 * This function assumes that the C standard random number generator have been 
 * formely initialized.
 */
BOOL registry_init_random(Registry* registry);

/**
 * \brief Load a registry from a file
 * \param[in] path The registry file path
 * \param[out] registry The loaded registry
 * \return REGLOAD_SUCCESS on success (the file exists and is properly formatted), a stricly negative 
 *         value characterizing the error (see <RegLoadResult> enum documentation for more details).
 */
RegLoadResult registry_load(char const* path, Registry* registry);

/**
 * \brief Save a registry to a file
 * \param[in] registry The registry
 * \param[in] path Path to the file in which the registry will be saved
 * \return TRUE if the save succeed, FALSE otherwise (IO error)
 */
BOOL registry_save(Registry* registry, char const* path);

/**
 * \brief Registers a new user with the specified name and a random unique ID 
 *        in a registry
 * \param[in] registry The registry
 * \param[in] name The user's name
 * \return The newly created user on success, NULL otherwise (out of memory)
 */
User* registry_create_user(Registry* registry, char const* name);

/**
 * \brief Loads the user with the specified ID registered in a registry
 * \param[in] registry The registry
 * \param[in] id The ID of the user
 * \param[out] user The loaded user on success
 * \return TRUE on success, NULL otherwise (the specified ID doesn't exist or out of memory)
 */
BOOL registry_load_user(Registry const* registry, unsigned long id, LoadedUser* user);

/**
 * \brief Free all the memory associated with a LoadedUser
 * \param[in] user The LoadedUser
 */
void registry_unload_user(LoadedUser* user);

/**
 * \brief Initializes an iterator traversing all the users registered in a registry
 * \param[in] registry The registry
 * \param[out] it The iterator to initialize
 */
void registry_get_user_list(Registry* registry, UsersIterator* it);

/**
 * \brief Performs a transaction request with the recipient user and the given amount
 * \param[in] registry The registry
 * \param[in] recipient The recipient user
 * \param[in] amount The amount (> 0.0)
 * \return The ID > 0 of the newly created transaction if the creation of the new transaction 
 *         succeed, 0 otherwise (out of memory or maximum number of transactions reached)
 *
 * A new transaction is registered in the registry with the status Pending.
 * 
 * This function assumes that the OpenSSL library and the C standard random number
 * generator have been formely initialized.
 */
unsigned long registry_request_transaction(Registry* registry, LoadedUser* recipient, double amount);

/**
 * \brief Confirms a pending transaction having the specified ID
 * \param[in] registry The registry
 * \param[in] emitter The emitter of the transaction
 * \param[in] id The ID of the pending transaction
 * \return The SHA-256 of the transaction key if the transaction exists, is Pending, is not expired, 
 *         the emitter balance allows it, and the emitter is not the recipient, NULL otherwise.
 *
 * If the confirmation of the transaction succeed, the transaction status becomes Confirmed.
 */
unsigned char const* registry_confirm_transaction(Registry* registry, LoadedUser* emitter, unsigned long id);

/**
 * \brief Executes a confirmed transaction having the specified ID
 * \param[in] registry The registry
 * \param[in] id The ID of the confirmed transaction
 * \param[in] recipient The recipient of the transaction
 * \param[in] key The transaction key
 * \return TRUE if the transaction exists, has been Confirmed, the transaction key is valid, and the
 *         given user is effectively the recipient of the transaction, FALSE otherwise
 *
 * If the execution of the transaction succeed, the end_date_time of the transaction is set to the 
 * current date and the transaction status becomes Executed.
 */
BOOL registry_execute_transaction(Registry* registry, LoadedUser* recipient, unsigned long id, unsigned char const* key);

/**
 * \brief Cancels a pending transaction having the specified ID
 * \param[in] registry The registry
 * \param[in] id The ID of the pending transaction
 * \return TRUE if the transaction exists and is Pending, FALSE otherwise
 */
BOOL registry_cancel_transaction(Registry* registry, unsigned long id);

/**
 * \brief Gets the last transaction date of the specified registry
 * \param[in] registry The registry
 * \return The last transaction date of the registry (timestamp in seconds)
 */
unsigned long registry_get_last_transaction_date(Registry* registry);

/**
 * \brief Returns the average transaction time between dates
 * \param[in] registry The registry
 * \param[in] start The start date (timestamp in seconds, inclusive)
 * \param[in] end The end date (timestamp in seconds, inclusive, end >= start)
 * \return The average transaction time between the two dates if there is at least one executed
 *         or canceled transaction between the two dates, a stricly negative number otherwise.
 *
 * Only executed and canceled transaction between the two specified dates are considered.
 */
double registry_get_average_transaction_time(Registry* registry, unsigned long start, unsigned long end);

/**
 * \brief Gets the transaction having the specified ID
 * \param[in] registry The registry
 * \param[in] id The ID of the transaction
 * \return The transaction having the specified ID if the latter exists, NULL otherwise
 */
Transaction const* registry_get_transaction(Registry const* registry, unsigned long id);

/**
 * \brief Gets the amount of the transaction having the specified ID
 * \param[in] registry The registry
 * \param[in] id The ID of the transaction
 * \return The amount of the transaction if the latter exists, a stricly negative value otherwise
 */
double registry_get_amount(Registry const* registry, unsigned long id);

/**
 * \brief Gets the status of the transaction having the specified ID
 * \param[in] registry The registry
 * \param[in] id The ID of the transaction
 * \return The status of the transaction if the latter exists, 0 otherwise
 */
TransactionStatus registry_get_status(Registry const* registry, unsigned long id);

/**
 * \brief Free all the memory associated with a registry
 * \param[in] registry The registry
 *
 * CAUTION: This function does not free all the memory associated with the loaded users.
 *          The latter must be freed with <registry_unload_user>.
 */
void registry_free(Registry* registry);

#endif /* REGISTRY_H */

