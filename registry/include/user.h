/**
 * \file
 * \brief   User of the centralized transaction registry
 * \date    Created:  29/10/2017
 * \date    Modified: 03/01/2018
 * \author  ALTENBACH Thomas (thomas.altenbach@utbm.fr)
 * \author  GUERET Alexis (alexis.gueret@utbm.fr)
 */

#ifndef USER_H
#define USER_H

#include <bool.h>
#include <ulskip_map.h>
#include <transactions_it.h>

/**
 * \brief Maximum length of a name
 */
#define USER_MAX_NAME_LENGTH 47

/**
 * \struct User
 * \brief A user of the centralized transaction registry
 */
typedef struct
{
	unsigned long id;       /**< The unique identifier of the user */
    char* name;             /**< The name of the user */
} User;

/**
 * \struct LoadedUser
 * \brief Wrapper holding a reference to a user, its balance and transactions
 */
typedef struct
{
	User* user;             /**< The user */
	double balance;  		/**< The balance of the user */
    ULSkipMap transactions; /**< The transactions of the user stored in a sorted map matching transactions with their creation dates */
} LoadedUser;

/**
 * \brief Creates a new user with the specified identifier and name
 * \param[out] user The newly created user
 * \param[in] id The user's unique identifier (> 0)
 * \param[in] name The user's name
 * \return TRUE on success, FALSE otherwise (invalid name or out of memory)
 *
 * A string is a valid name iff its length is lower than or equal to MAX_NAME_LENGTH and it
 * contains only alphanumeric characters and spaces.
 */
BOOL user_create(User* user, unsigned long id, char const* name);

/**
 * \brief Gets the ID of a user
 * \param[in] user The user
 * \return The ID of the user
 */
unsigned long user_get_id(User const* user);

/**
 * \brief Gets the name of a user
 * \param[in] user The user
 * \return The name of the user
 */
char const* user_get_name(User const* user);

/**
 * \brief Sets the name of a user
 * \param[in] user A pointer to the user
 * \param[in] name The new name of the user
 * \return TRUE on success, FALSE otherwise (invalid name or out of memory)
 *
 * A string is a valid name iff its length is lower than or equal to MAX_NAME_LENGTH and it
 * contains only alphanumeric characters and spaces.
 */
BOOL user_set_name(User* user, char const* name);

/**
 * \brief Gets the wrapped user of a LoadedUser wrapper
 * \param[in] user The LoadedUser
 * \return The wrapped user
 */
User* user_get_wrapped_user(LoadedUser* user);

/**
 * \brief Gets the ID of a wrapped user
 * \param[in] user The LoadedUser
 * \return The ID of the wrapped user
 */
unsigned long user_get_wrapped_id(LoadedUser* user);

/**
 * \brief Gets the name of a wrapped user
 * \param[in] user The LoadedUser
 * \return The name of the wrapped user
 */
char const* user_get_wrapped_name(LoadedUser* user);

/**
 * \brief Gets the balance of the user
 * \param[in] user The user
 * \return The balance of the user
 */
double user_get_balance(LoadedUser const* user);

/**
 * \brief Initializes an iterator traversing all the transactions of the user
 * \param[in] user The user
 * \param[out] it The iterator to initialize
 *
 * The transactions are returned in the descending order of creation date.
 */
void user_get_history(LoadedUser const* user, TransactionsIterator* it);

/**
 * \brief Initializes an iterator traversing all the transactions of the user created between 
 *        two specified dates.
 * \param[in] user The user
 * \param[in] start The start date (timestamp in seconds, inclusive)
 * \param[in] end The end date (timestamp in seconds, inclusive, end >= start)
 * \param[out] it The iterator to initialize
 *
 * The transactions are returned in the descending order of creation date.
 */
void user_get_history_limit(LoadedUser const* user, unsigned long start, unsigned long end, TransactionsIterator* it);

/**
 * \brief Gets the average credit of the user between two dates
 * \param[in] user The user
 * \param[in] start The start date (timestamp in seconds, inclusive)
 * \param[in] end The end date (timestamp in seconds, inclusive, end >= start)
 * \return The average credit of the user between the two dates if there is at least one credit
 *         between the two dates, zero otherwise.
 */
double user_get_average_credit(LoadedUser const* user, unsigned long start, unsigned long end);

/**
 * \brief Gets the average debit of the user between two dates
 * \param[in] user The user
 * \param[in] start The start date (timestamp in seconds, inclusive)
 * \param[in] end The end date (timestamp in seconds, inclusive, end >= start)
 * \return The average debit of the user between the two dates if there is at least one debit
 *         between the two dates, zero otherwise.
 */
double user_get_average_debit(LoadedUser const* user, unsigned long start, unsigned long end);

/**
 * \brief Serializes a user to a string
 * \param[in] user The user
 * \param[out] buffer The buffer into which the user must be serialized
 *
 * The buffer must be big enough to store the serialized user.
 */
void user_serialize(User const* user, char* buffer);

/**
 * \brief Deserializes a user from a string
 * \param[in] str The string
 * \param[out] user The deserialized user
 * \return TRUE on success, FALSE otherwise (the string is not a valid serialized user)
 *
 * The input string is modified during the process.
 */
BOOL user_deserialize(char* str, User* user);

/**
 * \brief Free all the memory associated with a user
 * \param[in] user The user
 */
void user_free(User* user);

#endif /* USER_H */

