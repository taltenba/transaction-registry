/**
 * \file
 * \brief   Iterator able to traverse a list of users
 * \date    Created:  01/12/2017
 * \date    Modified: 01/12/2017
 * \author  ALTENBACH Thomas (thomas.altenbach@utbm.fr)
 */

#ifndef USERS_IT_H
#define USERS_IT_H

#include <bool.h>
#include <user.h>
#include <id_map.h>

/**
 * \brief Iterator able to traverse a list of transactions
 */
typedef IDMapIterator UsersIterator;

/**
 * \brief Indicates whether or not there is a next element in an iteration
 * \param[in] it The iterator
 * \return TRUE if there is a next element to return, FALSE otherwise
 */
BOOL users_it_has_next(UsersIterator* it);

/**
 * \brief Indicates whether or not there is a previous element in an iteration
 * \param[in] it The iterator
 * \return TRUE if there is a previous element to return, FALSE otherwise
 */
BOOL users_it_has_prev(UsersIterator* it);

/**
 * \brief Gets the next user in the iteration
 * \param[in] it The iterator, having a next element
 * \return The next user in the iteration
 *
 * The iterator must have a next element.
 */
User* users_it_next(UsersIterator* it);

/**
 * \brief Gets the previous user in the iteration
 * \param[in] it The iterator, having a previous element
 * \return The previous user in the iteration
 *
 * The iterator must have a previous element.
 */
User* users_it_prev(UsersIterator* it);

#endif /* USERS_IT_H */

