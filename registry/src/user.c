/**
 * \file
 * \brief   User of the centralized transaction registry
 * \date    Created:  29/10/2017
 * \date    Modified: 03/01/2018
 * \author  ALTENBACH Thomas (thomas.altenbach@utbm.fr)
 * \author  GUERET Alexis (alexis.gueret@utbm.fr)
 */

#include <stdlib.h>
#include <stdio.h>
#include <user.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>
#include <ctype.h>
#include <limits.h>
#include <string_utils.h>

/**
 * \brief Go to the next token in the specified serialized string and abort execution by returning
 *        FALSE if there is no remaining token.
 * \param[in] str The string to cut the first time the macro is used, NULL afterwards
 * \param[out] token The variable in which to store the pointer to the beginning of the next token
 */
#define NEXT_TOKEN(str, token) do\
                               {\
                                   token = strtok(str, ";");\
                                   \
                                   if (token == NULL)\
                                       return FALSE;\
                               } while (0)

/**
 * \brief Returns a copy of the specified string if the latter is a valid name
 * \param[in] str The string
 * \return A copy of the specified string if the latter is a valid name and there is enough memory,
 *         NULL otherwise.
 *
 * A string is a valid name iff its length is stricly greater than 0, lower than or equal to 
 * USER_MAX_NAME_LENGTH and it contains only alphanumeric characters and spaces (' ').
 * Whitespaces at the beginning and the end of the string are removed.
 */
static char* copy_name(char const* str)
{
	size_t len;
	char* cpy;
	int i;
	
	while (isspace(*str))
		++str;
	
	len = strlen(str);
	
	while (isspace(str[len - 1]))
		--len;
	
	if (len == 0 || len > USER_MAX_NAME_LENGTH)
		return NULL;
		
	cpy = malloc(len + 1);
	
	if (cpy == NULL)
		return NULL;
	
	i = 0;
	
	while (i < len && (isalnum(str[i]) || str[i] == ' '))
	{
		cpy[i] = str[i];
		++i;
	}
	
	if (i < len)    /* Invalid name */
	{
		free(cpy);
		return NULL;
	}
	
	cpy[len] = '\0';
	
	return cpy;
}

BOOL user_create(User* user, unsigned long id, char const* name)
{
	char* name_cpy;
    
    assert(user != NULL);
    assert(name != NULL);
    assert(id != 0);
    
    name_cpy = copy_name(name);
    
	if (name_cpy == NULL)
		return FALSE;
	
	user->id = id;
    user->name = name_cpy;
	
    return TRUE;
}

unsigned long user_get_id(User const* user)
{
	assert(user != NULL);
    return user->id;
}

char const* user_get_name(User const* user)
{
	assert(user != NULL);
    return user->name;
}

BOOL user_set_name(User* user, char const* name)
{
	char* new_name;
	
	assert(user != NULL);
	assert(name != NULL);
	
	new_name = copy_name(name);
	
	if (new_name == NULL)
		return FALSE;
	
	free(user->name);
	
    user->name = new_name;
    
    return TRUE;
}

User* user_get_wrapped_user(LoadedUser* user)
{
	assert(user != NULL);
	return user->user;
}

/**
 * \brief Gets the ID of a wrapped user
 * \param[in] user The LoadedUser
 * \return The ID of the wrapped user
 */
unsigned long user_get_wrapped_id(LoadedUser* user)
{
	assert(user != NULL);
	return user->user->id;
}

/**
 * \brief Gets the name of a wrapped user
 * \param[in] user The LoadedUser
 * \return The name of the wrapped user
 */
char const* user_get_wrapped_name(LoadedUser* user)
{
	assert(user != NULL);
	return user->user->name;
}


double user_get_balance(LoadedUser const* user)
{
	assert(user != NULL);
	return user->balance;
}

void user_get_history(LoadedUser const* user, TransactionsIterator* it)
{
	assert(user != NULL);
	assert(it != NULL);
	
	ulsmap_get_between_reverse(&user->transactions, 0, ULONG_MAX, it);
}

void user_get_history_limit(LoadedUser const* user, unsigned long start, unsigned long end, TransactionsIterator* it)
{
	assert(user != NULL);
	assert(it != NULL);
	assert(start <= end);
	
	ulsmap_get_between_reverse(&user->transactions, start, end, it);
}

/**
 * \brief Computes the average amount of the transactions of the given user that have been created 
 *        between the two specified dates and validate the given predicate.
 * \param[in] user The user
 * \param[in] start The start date (timestamp in seconds, inclusive)
 * \param[in] end The end date (timestamp in seconds, inclusive)
 * \param[in] predicate The predicate (returning TRUE if the transaction must be considered for the
 *                      computation, FALSE otherwise).
 * \return The average amount of the user transactions created between the two dates that 
 *         validate the predicate if there is a least one transaction validating it, zero otherwise.
 *
 * The second argument of the predicate is the user ID.
 */
static double get_average_amount(LoadedUser const* user, unsigned long start, unsigned long end, BOOL (*predicate)(Transaction*, unsigned long))
{
	ULSkipMapIterator it;
	Transaction* cur;
	unsigned long user_id;
	double sum;
	size_t count;
	
	ulsmap_get_between(&user->transactions, start, end, &it);
	
	user_id = user->user->id;
	sum = 0.0;
	count = 0;
	
	while (ulsmap_it_has_next(&it))
	{
		cur = (Transaction*) ulsmap_it_next(&it);
		
		if (predicate(cur, user_id))
		{
			sum += cur->amount;
			++count;
		}
	}
	
	if (count == 0)
		return 0.0;
	
	return sum / count;
}

/**
 * \brief Indicates whether or not the specified transaction is a credit for the specified user.
 * \param[in] transaction The transaction
 * \param[in] user_id The user id
 * \return TRUE iff the transaction status is EXECUTED and the recipient of the transaction
 *         is the specified user. 
 */
static BOOL is_credit(Transaction* transaction, unsigned long user_id)
{
	return transaction->status == EXECUTED && transaction->recipient_id == user_id;
}

/**
 * \brief Indicates whether or not the specified transaction is a debit for the specified user.
 * \param[in] transaction The transaction
 * \param[in] user_id The user id
 * \return TRUE iff the transaction status is EXECUTED and the emitter of the transaction
 *         is the specified user. 
 */
static BOOL is_debit(Transaction* transaction, unsigned long user_id)
{
	return transaction->status == EXECUTED && transaction->emitter_id == user_id;
}

double user_get_average_credit(LoadedUser const* user, unsigned long start, unsigned long end)
{
	assert(user != NULL);
	assert(start <= end);
	return get_average_amount(user, start, end, is_credit);
}

double user_get_average_debit(LoadedUser const* user, unsigned long start, unsigned long end)
{
	assert(user != NULL);
	assert(start <= end);
	return get_average_amount(user, start, end, is_debit);
}

void user_serialize(User const* user, char* buffer)
{
	assert(user != NULL);
	assert(buffer != NULL);
	
	/* Since we compile in ANSI C, we sadly cannot use the far more secured snprintf */
	sprintf(buffer, "%lu;%s", user->id, user->name);
}

BOOL user_deserialize(char* str, User* user)
{
	char* token;
	
	assert(str != NULL);
	assert(user != NULL);
	
	NEXT_TOKEN(str, token);
	
	if (!parse_ulong(token, &user->id) || user->id == 0)
		return FALSE;
	
	NEXT_TOKEN(NULL, token);
	
	user->name = copy_name(token);
	
	return user->name != NULL;
}

void user_free(User* user)
{
	assert(user != NULL);
	free(user->name);
}
