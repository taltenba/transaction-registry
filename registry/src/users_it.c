/**
 * \file
 * \brief   Iterator able to traverse a list of users
 * \date    Created:  01/12/2017
 * \date    Modified: 01/12/2017
 * \author  ALTENBACH Thomas (thomas.altenbach@utbm.fr)
 */

#include <id_map.h>
#include <assert.h>
#include <users_it.h>

BOOL users_it_has_next(UsersIterator* it)
{
	assert(it != NULL);
	return idmap_it_has_next(it);
}

BOOL users_it_has_prev(UsersIterator* it)
{
	assert(it != NULL);
	return idmap_it_has_prev(it);
}

User* users_it_next(UsersIterator* it)
{
	assert(it != NULL);
	return (User*) idmap_it_next(it);
}

User* users_it_prev(UsersIterator* it)
{
	assert(it != NULL);
	return (User*) idmap_it_prev(it);
}
