/**
 * \file
 * \brief   Iterator able to traverse a list of transactions
 * \date    Created:  01/12/2017
 * \date    Modified: 01/12/2017
 * \author  ALTENBACH Thomas (thomas.altenbach@utbm.fr)
 */

#include <ulskip_map.h>
#include <assert.h>
#include <transactions_it.h>

BOOL transactions_it_has_next(TransactionsIterator const* it)
{
	assert(it != NULL);
	return ulsmap_it_has_prev(it);               /* Reverse order */
}

BOOL transactions_it_has_prev(TransactionsIterator const* it)
{
	assert(it != NULL);
	return ulsmap_it_has_next(it);               /* Reverse order */
}

Transaction* transactions_it_next(TransactionsIterator* it)
{
	assert(it != NULL);
	return (Transaction*) ulsmap_it_prev(it);    /* Reverse order */
}

Transaction* transactions_it_prev(TransactionsIterator* it)
{
	assert(it != NULL);
	return (Transaction*) ulsmap_it_next(it);    /* Reverse order */
}
