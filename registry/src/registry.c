/**
 * \file
 * \brief   Centralized and secured transaction registry
 * \date    Created:  29/10/2017
 * \date    Modified: 03/01/2018
 * \author  ALTENBACH Thomas (thomas.altenbach@utbm.fr)
 *
 * Transaction registry keeping track of monetary exchanges between different actors in addition to 
 * secure the transactions.
 *
 * Each transaction is defined in particular by two users, one called recipient (the one who will
 * receive the money) and an emitter (the one who will send the money).
 *
 * The registry is intended to secure this transaction in that avoid ensure the emitter has a sufficient
 * balance to complete the transaction, preventing the emitter from giving the recipient an invalid 
 * payment solution.
 *
 * To achieve this aim the transaction process follows three phases:
 *     - The recipient performs a transaction request in the registry, specifying the amount, and will 
 *       be given a transaction ID. The created transaction is then "PENDING".
 *     - The recipient gives that ID to the emitter and the latter has to confirmed the transaction
 *       before it expired. If the emitter has enough money the transaction amount is withdrawn from its
 *       balance. In that case the transaction will be "CONFIRMED" and a private transaction key will be
 *       given to the emitter. After the transaction has been confirmed it cannot be canceled anymore.
 *     - During the exchange, in order to pay the recipient and complete the transaction, the emitter 
 *       gives the key to the recipient. Having that key the latter will then be able to execute the 
 *       transaction and its balance will be credited with the transaction amount.
 *
 * Thus the registry allows the recipient to check instantaneously the validity of the payment solution
 * before to complete the exchange.
 *
 * This registry implementation can store at most (2^32 - 1) transactions and the same number of users.
 */

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <registry.h>
#include <rnd_utils.h>
#include <file_utils.h>
#include <time.h>

/**
 * \brief Maximum number of unique ids that can be generated
 *
 * C standard only guarantees that at least 2^32 distinct integers can be stored 
 * in an unsigned long and we do not use the zero id.
 */
#define MAX_UNIQUE_IDS_COUNT 4294967295

/**
 * \brief Capacity of one memory chunk in the transaction elements memory pool
 */
#define TRANSACTIONS_MEMPOOL_CHUNK_CAPACITY 128

/**
 * \brief Capacity of one memory chunk in the users memory pool
 */
#define USERS_MEMPOOL_CHUNK_CAPACITY 128

/**
 * \brief Size of the buffer used to perform IO operations with the registry file
 */
#define IO_BUFFER_SIZE 2048

/**
 * \brief Number of users created during the random initialization of a new registry
 *
 * Must be a power of two.
 */
#define RND_USER_COUNT 64

/**
 * \brief The exponent of the power of two corresponding to RND_USER_COUNT
 */
#define RND_USER_COUNT_POW2_EXP 6

/**
 * \brief Initial balance of the users created during the random initialization of a new registry 
 *        registry.
 */
#define RND_USER_INIT_BALANCE 4000

/**
 * \brief Number of transactions created during the random initialization of a new registry
 */
#define RND_TRANSAC_COUNT 10000


/**
 * \brief Minimum amount of a random transaction created during the random initialization of a new 
 *        registry (inclusive).
 */
#define MIN_RND_TRANSAC_AMOUNT 10

/**
 * \brief Maximum amount of a random transaction created during the random initialization of a new 
 *        registry (exclusive).
 */
#define MAX_RND_TRANSAC_AMOUNT 100

/**
 * \brief Duration between two transactions created during the random initialization of a new 
 *        registry (in seconds).
 */
#define RND_TIME_INC 60

/**
 * \brief Minimum time between the creation and execution of a random transaction created during
 *        the random initialization of a new registry (in seconds).
 */
#define MIN_RND_TRANSAC_TIME 60

/**
 * \brief Initializes a new empty registry
 * \param[out] registry The registry to initialize
 * \return TRUE on success, FALSE otherwise (out of memory)
 */
static BOOL init_registry(Registry* registry)
{
	if (!idmap_init(&registry->transactions_ids_map, 0))
		return FALSE;
		
	if (!idmap_init(&registry->users_ids_map, 0))
	{
		idmap_free(&registry->transactions_ids_map);
		return FALSE;
	}
	
	if (!ulsmap_init(&registry->transactions_dates_map, 0))
	{
		idmap_free(&registry->transactions_ids_map);
		idmap_free(&registry->users_ids_map);
		return FALSE;
	}
	
	fsmpool_init(&registry->transactions_pool, sizeof(TransactionElement), 
	             TRANSACTIONS_MEMPOOL_CHUNK_CAPACITY, TRANSACTIONS_MEMPOOL_CHUNK_CAPACITY);
	
	fsmpool_init(&registry->users_pool, sizeof(User), 
	             USERS_MEMPOOL_CHUNK_CAPACITY, USERS_MEMPOOL_CHUNK_CAPACITY);
	
	registry->head = NULL;
	registry->tail = NULL;
	registry->transaction_count = 0;
	registry->last_transaction_date = 0UL;
	
	/* Seed the id generators with random values */
	registry->last_transaction_id = rand();
	registry->last_user_id = rand();
	
	return TRUE;
}

/**
 * \brief Add a new transaction to the registry
 * \param[in] registry The registry
 * \param[in] new_elem New element to add to the registry
 * \return TRUE on success, FALSE otherwise (out of memory).
 */
static BOOL add_to_registry(Registry* registry, TransactionElement* new_elem)
{
	Transaction* transaction;
	
	transaction = &new_elem->transaction;
	
	if (!idmap_put(&registry->transactions_ids_map, transaction->id, transaction))
		return FALSE;
		
	if (!ulsmap_put(&registry->transactions_dates_map, transaction->start_date_time, transaction))
	{
		idmap_remove(&registry->transactions_ids_map, transaction->id);
		return FALSE;
	}
	
	if (registry->head == NULL)
	{
		registry->head = new_elem;
		new_elem->prev = NULL;
	}
	else
	{
		registry->tail->next = new_elem;
		new_elem->prev = registry->tail;
	}
	
	registry->tail = new_elem;
	new_elem->next = NULL;
	
	registry->transaction_count++;
	registry->last_transaction_date = new_elem->transaction.start_date_time;
	
	return TRUE;
}

/**
 * \brief Generate a random unique identifier given the last generated id
 * \param[in] last_id The last generated id
 * \return A random unique identifier between 1 (inclusive) and 2^32 (exclusive)
 */
static unsigned long rnd_id(unsigned long last_id)
{
	unsigned long id = rnd_next_unique(last_id);
	
	/* We do not use the zero id */
	if (id == 0UL)
		id = rnd_next_unique(0UL);
	
	return id;
}

/**
 * \brief Creates a new pending transaction with the specified recipient, amount and start date time, 
 *        and add it to the registry.
 * \param[in] registry The registry
 * \param[in] recipient The transaction recipient
 * \param[in] amount The amount of the transaction
 * \param[in] start_date_time The start date time of the transaction
 * \return TRUE on success, FALSE otherwise (out of memory)
 */
static Transaction* new_transaction(Registry* registry, User* recipient, double amount, unsigned long start_date_time)
{
	TransactionElement* transac_elem;
	Transaction* transac;
	BOOL success;
	
	transac_elem = fsmpool_alloc(&registry->transactions_pool);
	
	if (transac_elem == NULL)
		return FALSE;
		
	transac = &transac_elem->transaction;
	
	success = transaction_create(transac, rnd_id(registry->last_transaction_id), recipient->id, 
	                             amount, start_date_time) &&
			  add_to_registry(registry, transac_elem);
			  
	if (!success)
	{
		fsmpool_free(&registry->transactions_pool, transac_elem);
		return FALSE;
	}
	
	registry->last_transaction_id = transac->id;
	
	return transac;
}

/**
 * \brief Creates RND_USER_COUNT users in a registry
 * \param[in] registry The registry
 * \param[out] users The created users
 * \return TRUE on success, FALSE otherwise (out of memory)
 */
static BOOL init_random_users(Registry* registry, User** users)
{
	size_t i;
	char user_name_buffer[8];
	BOOL success;
	
	i = 0;
	success = TRUE;
	
	while (i < RND_USER_COUNT && success)
	{
		sprintf(user_name_buffer, "User %lu", i + 1);
		users[i] = registry_create_user(registry, user_name_buffer);
		
		success = users[i] != NULL;
		++i;
	}
	
	return success;
}

/**
 * \brief Gives to RND_USER_COUNT users an initial balance of RND_USER_INIT_BALANCE
 * \param[in] registry The registry
 * \param[in] users The users
 * \param[in] start_init_time The timestamp in seconds of the first initialization : the i-th user
 *                            will have its balance initialized at (start_init_time + i) seconds.
 * \param[out] balances Array containing the initial balance of each user
 * \return TRUE on success, FALSE otherwise (out of memory)
 */
static BOOL init_random_user_balances(Registry* registry, User** users, unsigned long start_init_time, double* balances)
{
	Transaction* transac;
	size_t i;
	BOOL success;
	
	i = 0;
	success = TRUE;
	
	while (i < RND_USER_COUNT && success)
	{
		transac = new_transaction(registry, users[i], RND_USER_INIT_BALANCE, start_init_time);
		
		if (transac == NULL)
			success = FALSE;
		else
		{
			balances[i] = RND_USER_INIT_BALANCE;
			transac->emitter_id = 0;
		 	transac->end_date_time = start_init_time;
		 	transac->status = EXECUTED;
		 	
		 	++start_init_time;
		 	++i;
		}
	}
	
	return success;
}

/**
 * \brief Generates and adds a new random transaction to the registry
 * \param[in] registry The registry
 * \param[in] users RND_USER_COUNT users of the registry
 * \param[in] balances The balances of the users
 * \param[in] transac_time The date of the transaction
 * \param[in] cur_date The current date
 * \return TRUE on success, FALSE otherwise (out of memory)
 */
static BOOL new_random_transaction(Registry* registry, User** users, double* balances, 
                                   unsigned long transac_date, unsigned long cur_date)
{
	/* Higher chance to create an executed transaction */
	static TransactionStatus possible_status[4] = {PENDING, CONFIRMED, EXECUTED, EXECUTED};
	
	unsigned long recipient_index;
	unsigned long emitter_index;
	double amount;
	User* recipient;
	User* emitter;
	Transaction* transac;
	
	recipient_index = rnd_pow2(RND_USER_COUNT_POW2_EXP);
	recipient = users[recipient_index];
	
	amount = rnd_between(MIN_RND_TRANSAC_AMOUNT, MAX_RND_TRANSAC_AMOUNT);
	
	transac = new_transaction(registry, recipient, amount, transac_date);
	
	if (transac == NULL)
		return FALSE;
	
	transac->status = possible_status[rnd_pow2(2)];
	
	if (transac->status == PENDING)
	{
		if (transaction_is_expired(transac, cur_date))
		{
			transac->status = CANCELED;
			transac->end_date_time = transac_date + transac->ttl + 1;
		}
	}
	else
	{
		do
		{
			emitter_index = rnd_pow2(RND_USER_COUNT_POW2_EXP);
			emitter = users[emitter_index];
		} while (emitter->id == transac->recipient_id);
		
		transac->emitter_id = emitter->id;
		
		if (amount > balances[emitter_index])
			transac->status = CANCELED;
		else
			balances[emitter_index] -= amount;
			
		if (transac->status == EXECUTED)
			balances[recipient_index] += amount;
		
		transac->end_date_time = rnd_between(transac_date + MIN_RND_TRANSAC_TIME, cur_date);	
	}
	
	return TRUE;
}

/**
 * \brief Generates and adds RND_TRANSAC_COUNT random transactions to the registry
 * \param[in] registry The registry
 * \param[in] users RND_USER_COUNT users of the registry
 * \return TRUE on success, FALSE otherwise (out of memory)
 *
 * The initial balance of each user is set to RND_USER_INIT_BALANCE.
 */
static BOOL init_random_transactions(Registry* registry, User** users)
{
	int i;
	unsigned long cur_time;
	unsigned long transac_time;
	double balances[RND_USER_COUNT];
	
	cur_time = time(NULL);
	transac_time = cur_time - 
	              (RND_TRANSAC_COUNT * RND_TIME_INC + MIN_RND_TRANSAC_TIME + RND_USER_COUNT + 1);
	
	init_random_user_balances(registry, users, transac_time, balances);
	
	transac_time += RND_USER_COUNT;
	
	i = 0;
	
	while (i < RND_TRANSAC_COUNT && new_random_transaction(registry, users, balances, transac_time, cur_time))
	{
		transac_time += RND_TIME_INC;
		++i;
	}
	
	if (i != RND_TRANSAC_COUNT)
		return FALSE;
	
	return TRUE;	
}

BOOL registry_init_random(Registry* registry)
{
	User* users[RND_USER_COUNT];
	
	assert(registry != NULL);
	
	if (!init_registry(registry))
		return FALSE;
	
	if (!init_random_users(registry, users) || !init_random_transactions(registry, users))
	{
		registry_free(registry);
		return FALSE;
	}
	
	return TRUE;
}

/**
 * \brief Free all the memory related to the users of a registry
 * \param[in] registry The registry
 */
static void free_users(Registry* registry)
{
	IDMapIterator it;
	
	idmap_it_init(&registry->users_ids_map, &it);
	
	while (idmap_it_has_next(&it))
		user_free((User*) idmap_it_next(&it));
	
	idmap_free(&registry->users_ids_map);
	fsmpool_clear(&registry->users_pool);
}

/**
 * \brief Free all the memory related to the transactions of a registry
 * \param[in] registry The registry
 */
static void free_transactions(Registry* registry)
{
	idmap_free(&registry->transactions_ids_map);
	ulsmap_free(&registry->transactions_dates_map);
	fsmpool_clear(&registry->transactions_pool);
}

/**
 * \brief Load the users of a registry from a file
 * \param[in] registry The registry
 * \param[in] file The file
 * \param[in] buffer Buffer of size IO_BUFFER_SIZE used to read the file
 * \param[in] users_count The number of users to load in the file
 * \return REGLOAD_SUCCESS on success, a stricly negative value characterizing the error (see 
 *         <RegLoadResult> enum documentation for more details).
 */
static RegLoadResult load_users(Registry* registry, FILE* file, char* buffer, size_t user_count)
{
	size_t i;
	User* new_user;
	RegLoadResult result;
	
	if (!idmap_init(&registry->users_ids_map, user_count))
		return REGLOAD_OUT_OF_MEMORY;
    
    fsmpool_init(&registry->users_pool, sizeof(User), USERS_MEMPOOL_CHUNK_CAPACITY, user_count);
    
    i = 0;
    result = REGLOAD_SUCCESS;
    
	while (result == REGLOAD_SUCCESS && i < user_count && fgets(buffer, IO_BUFFER_SIZE, file) != NULL)
	{
		new_user = fsmpool_alloc(&registry->users_pool);
		
		if (new_user == NULL)
		{
			result = REGLOAD_OUT_OF_MEMORY;
		}
		else if (!user_deserialize(buffer, new_user))
		{
			fsmpool_free(&registry->users_pool, new_user);
			result = REGLOAD_INVALID_FILE;
		}
		else if (!idmap_put(&registry->users_ids_map, new_user->id, new_user))
		{
			fsmpool_free(&registry->users_pool, new_user);
			result = REGLOAD_OUT_OF_MEMORY;
		}
		
		++i;
	}
	
	if (result != REGLOAD_SUCCESS || i < user_count)
	{
		free_users(registry);
		return result;
	}
	
	return REGLOAD_SUCCESS;
}

/**
 * \brief Load the next transaction of a registry from a file
 * \param[in] registry The registry
 * \param[in] file The file
 * \param[in] buffer Buffer of size IO_BUFFER_SIZE used to read the file
 * \param[out] transac_elem The newly loaded transaction element
 * \return REGLOAD_SUCCESS on success, a stricly negative value characterizing the error (see 
 *         <RegLoadResult> enum documentation for more details).
 *
 * The users of the registry must have been formerly loaded.
 * The loaded transaction is added to the recipient and emitter list of transactions.
 */
static RegLoadResult load_next_transaction(Registry* registry, FILE* file, char* buffer, TransactionElement** transac_elem)
{
	Transaction* transaction;
	BOOL error;
	
	*transac_elem = fsmpool_alloc(&registry->transactions_pool);
	
	if (transac_elem == NULL)
		return REGLOAD_OUT_OF_MEMORY;
	
	transaction = &(*transac_elem)->transaction;
	
	error = fgets(buffer, IO_BUFFER_SIZE, file) == NULL ||
	        !transaction_deserialize(buffer, transaction);
	
	if (error)
	{
		fsmpool_free(&registry->transactions_pool, *transac_elem);
		return REGLOAD_INVALID_FILE;
	}
		
	error = !idmap_put(&registry->transactions_ids_map, transaction->id, transaction) ||
	        !ulsmap_put(&registry->transactions_dates_map, transaction->start_date_time, transaction);
	
	if (error)
		return REGLOAD_OUT_OF_MEMORY;
	
	return REGLOAD_SUCCESS;
}

/**
 * \brief Load the transactions of a registry from a file
 * \param[in] registry The registry
 * \param[in] file The file
 * \param[in] buffer Buffer of size IO_BUFFER_SIZE used to read the file
 * \return REGLOAD_SUCCESS on success, a stricly negative value characterizing the error (see 
 *         <RegLoadResult> enum documentation for more details).
 *
 * The users of the registry must have been formerly loaded.
 * The transactions are added to the recipient and emitter list of transactions.
 */
static RegLoadResult load_transactions(Registry* registry, FILE* file, char* buffer)
{
	size_t i;
	size_t transaction_count;
	TransactionElement* prev;
    TransactionElement* cur;
	RegLoadResult result;
	
	transaction_count = registry->transaction_count;
	
	if (!idmap_init(&registry->transactions_ids_map, transaction_count))
		return REGLOAD_OUT_OF_MEMORY;
		
	if (!ulsmap_init(&registry->transactions_dates_map, transaction_count))
	{
		idmap_free(&registry->transactions_ids_map);
		return REGLOAD_OUT_OF_MEMORY;
	}
	
	fsmpool_init(&registry->transactions_pool, sizeof(TransactionElement),
	             TRANSACTIONS_MEMPOOL_CHUNK_CAPACITY, transaction_count);
	
	if (transaction_count == 0)
	{
		registry->head = NULL;
		registry->tail = NULL;
		return REGLOAD_SUCCESS;
	}
	
	result = load_next_transaction(registry, file, buffer, &cur);
	
	if (result != REGLOAD_SUCCESS)
    {
        free_transactions(registry);
        return result;
    }
    
    cur->prev = NULL;
    registry->head = cur;
    prev = cur;
	
	i = 1;
	
	while (result == REGLOAD_SUCCESS && i < transaction_count)
	{
		result = load_next_transaction(registry, file, buffer, &cur);
		
		if (result == REGLOAD_SUCCESS)
		{
			cur->prev = prev;
			prev->next = cur;
			
			prev = cur;
			++i;
		}
	}
	
	if (result != REGLOAD_SUCCESS)
    {
        free_transactions(registry);
        return result;
    }
	
	if (i < transaction_count)
	{
		free_transactions(registry);
        return REGLOAD_INVALID_FILE;
	}
	
	cur->next = NULL;
	registry->tail = cur;
	
	return REGLOAD_SUCCESS;
}

RegLoadResult registry_load(char const* path, Registry* registry)
{
	FILE* file;
	size_t user_count;
    char buffer[IO_BUFFER_SIZE];
    BOOL error;
    RegLoadResult result;
    
    file = fopen(path, "r");
    
	if (file == NULL)
    	return REGLOAD_FILE_NOT_FOUND;
    
	error = !file_read_ulong(file, buffer, IO_BUFFER_SIZE, &registry->last_transaction_date) ||
	        !file_read_ulong(file, buffer, IO_BUFFER_SIZE, &registry->last_transaction_id) ||
	        !file_read_ulong(file, buffer, IO_BUFFER_SIZE, &registry->last_user_id) ||
	        !file_read_ulong(file, buffer, IO_BUFFER_SIZE, &registry->transaction_count) ||
	        !file_read_ulong(file, buffer, IO_BUFFER_SIZE, &user_count);
	
	if (error)
	{
		fclose(file);
		return REGLOAD_INVALID_FILE;
	}
    
    result = load_users(registry, file, buffer, user_count);
	
	if (result != REGLOAD_SUCCESS)
	{
		fclose(file);
		return result;
	}
	
	result = load_transactions(registry, file, buffer);
	
    if (result != REGLOAD_SUCCESS)
    	free_users(registry);
    	
    fclose(file);
    
    return result; 
}

BOOL registry_save(Registry* registry, char const* path)
{
	FILE* file;
    char buffer[IO_BUFFER_SIZE];
    IDMapIterator user_it;
    TransactionElement* cur_elem;
    BOOL error;
    
    assert(registry != NULL);
    assert(path != NULL);
    
    file = fopen(path, "w");

    if (file == NULL)
        return FALSE;
        
    error = 0 > fprintf(file, "%lu\n%lu\n%lu\n%lu\n%lu\n", registry->last_transaction_date,
                                                           registry->last_transaction_id,
                                                           registry->last_user_id,
                                                           registry->transaction_count,
                                                           idmap_count(&registry->users_ids_map));
    if (error)
    {
        fclose(file);
        return FALSE;
    }
    
    idmap_it_init(&registry->users_ids_map, &user_it);
    
    while (!error && idmap_it_has_next(&user_it))
    {
    	user_serialize(idmap_it_next(&user_it), buffer);
    	error = fprintf(file, "%s\n", buffer) < 0;
    }
    
    if (error)
    {
        fclose(file);
        return FALSE;
    }
    
    cur_elem = registry->head;
    
    while (!error && cur_elem != NULL)
    {
    	transaction_serialize(&cur_elem->transaction, buffer);
    	error = fprintf(file, "%s\n", buffer) < 0;
    	cur_elem = cur_elem->next;
    }
    
    fclose(file);
    
    return !error;
}

User* registry_create_user(Registry* registry, char const* name)
{
	User* new_user;
	unsigned long new_id;
	
	assert(registry != NULL);
	assert(name != NULL);
	
	new_user = fsmpool_alloc(&registry->users_pool);
	
	if (new_user == NULL)
		return NULL;
	
	new_id = rnd_id(registry->last_user_id);
	
	if (!user_create(new_user, new_id, name))
	{
		fsmpool_free(&registry->users_pool, new_user);
		return NULL;
	}
	
	if (!idmap_put(&registry->users_ids_map, new_id, new_user))
	{
		user_free(new_user);
		fsmpool_free(&registry->users_pool, new_user);
		return NULL;
	}
	
	registry->last_user_id = new_id;
	
	return new_user;
}

BOOL registry_load_user(Registry const* registry, unsigned long id, LoadedUser* user)
{
	User* to_load_user;
	TransactionElement* cur_elem;
	Transaction* cur_transac;
	double balance;
	BOOL success;
	
	assert(registry != NULL);
	assert(user != NULL);
	
	to_load_user = idmap_get(&registry->users_ids_map, id);
	
	if (to_load_user == NULL)
		return FALSE;
		
	if (!ulsmap_init(&user->transactions, 0))
		return FALSE;
		
	user->user = to_load_user;
	
	cur_elem = registry->head;
	balance = 0.0;
	success = TRUE;
	
	while (cur_elem != NULL && success)
	{
		cur_transac = &cur_elem->transaction;
		
		if (cur_transac->emitter_id == id || cur_transac->recipient_id == id)
		{
			if (!ulsmap_put(&user->transactions, cur_transac->start_date_time, cur_transac))
			{
				success = FALSE;
			}
			else
			{
				if ((cur_transac->status == CONFIRMED || cur_transac->status == EXECUTED) && cur_transac->emitter_id == id)
					balance -= cur_transac->amount;
				else if (cur_transac->status == EXECUTED && cur_transac->recipient_id == id)
					balance += cur_transac->amount;
			}
		}
		
		cur_elem = cur_elem->next;
	}
	
	if (!success)
	{
		ulsmap_free(&user->transactions);
		return FALSE;
	}
	
	user->balance = balance;
	return TRUE;
}

void registry_unload_user(LoadedUser* user)
{
	assert(user != NULL);
	ulsmap_free(&user->transactions);
}

void registry_get_user_list(Registry* registry, UsersIterator* it)
{
	assert(registry != NULL);
	assert(it != NULL);
	
	idmap_it_init(&registry->users_ids_map, it);
}

unsigned long registry_request_transaction(Registry* registry, LoadedUser* recipient, double amount)
{
    TransactionElement* new_elem;
	Transaction* new_transaction;
	
	assert(registry != NULL);
	assert(recipient != NULL);
	assert(amount > 0.0);
	
	/* 
	 * Currently our skip map implementation is not managing duplicates so we do not allow two 
	 * transactions to be made at the same timestamp.
	 */
	if (registry->transaction_count == MAX_UNIQUE_IDS_COUNT || registry->last_transaction_date == time(NULL))
		return 0;
	
	new_elem = fsmpool_alloc(&registry->transactions_pool);
	
	if (new_elem == NULL)
		return 0;
	
	new_transaction = &new_elem->transaction;
	
    if (!transaction_create(new_transaction, rnd_id(registry->last_transaction_id), recipient->user->id, amount, time(NULL)))
	{
		fsmpool_free(&registry->transactions_pool, new_elem);
		return 0;
	}
	
	if (!ulsmap_put(&recipient->transactions, new_transaction->start_date_time, new_transaction))
	{
		fsmpool_free(&registry->transactions_pool, new_elem);
		return 0;
	}
	
	if (!add_to_registry(registry, new_elem))
	{
		fsmpool_free(&registry->transactions_pool, new_elem);
		ulsmap_remove(&recipient->transactions, new_transaction->start_date_time);
		return 0;
	}
	
	registry->last_transaction_id = new_transaction->id;
	
	return new_transaction->id;
}

unsigned char const* registry_confirm_transaction(Registry* registry, LoadedUser* emitter, unsigned long id)
{
	Transaction* transaction;
	unsigned long cur_time;
	
	assert(registry != NULL);
	assert(emitter != NULL);
	
	transaction = idmap_get(&registry->transactions_ids_map, id);
	
	if (transaction == NULL || transaction->status != PENDING || emitter->user->id == transaction->recipient_id)
		return NULL;
	
	cur_time = time(NULL);
	
	if (transaction_is_expired(transaction, cur_time))
	{
		transaction->status = CANCELED;
		transaction->end_date_time = cur_time;
		return NULL;
	}
	
	if (emitter->balance < transaction->amount)
		return NULL;
	
	if (!ulsmap_put(&emitter->transactions, transaction->start_date_time, transaction))
		return NULL;
	
	emitter->balance -= transaction->amount;
	
	transaction->emitter_id = emitter->user->id;
	transaction->status = CONFIRMED;
	
	return transaction->private_key;
}

BOOL registry_execute_transaction(Registry* registry, LoadedUser* recipient, unsigned long id, unsigned char const* key)
{
	Transaction* transaction;
	
	assert(registry != NULL);
	assert(recipient != NULL);
	assert(key != NULL);
	
	transaction = idmap_get(&registry->transactions_ids_map, id);
	
	if (transaction == NULL || transaction->status != CONFIRMED || 
	    transaction->recipient_id != recipient->user->id ||
	    !transaction_check_key(transaction, key))
	{
		return FALSE;
	}
	
	recipient->balance += transaction->amount;
	
	transaction->status = EXECUTED;
	transaction->end_date_time = time(NULL);
	
	return TRUE;
}

BOOL registry_cancel_transaction(Registry* registry, unsigned long id)
{
	Transaction* transaction;
	
	assert(registry != NULL);
	
	transaction = idmap_get(&registry->transactions_ids_map, id);
	
	if (transaction == NULL || transaction->status != PENDING)
		return FALSE;
		
	transaction->status = CANCELED;
	transaction->end_date_time = time(NULL);
	
	return TRUE;
}

unsigned long registry_get_last_transaction_date(Registry* registry)
{
	assert(registry != NULL);
	return registry->last_transaction_date;
}

double registry_get_average_transaction_time(Registry* registry, unsigned long start, unsigned long end)
{
	ULSkipMapIterator it;
	Transaction* cur;
	double sum;
	size_t count;
	
	assert(registry != NULL);
	assert(start <= end);
	
	ulsmap_get_between(&registry->transactions_dates_map, start, end, &it);
	
	sum = 0.0f;
	count = 0;
	
	while (ulsmap_it_has_next(&it))
	{
		cur = (Transaction*) ulsmap_it_next(&it);
		
		if (cur->status == EXECUTED || cur->status == CANCELED)
		{
			sum += cur->end_date_time - cur->start_date_time;
			++count;
		}
	}
	
	if (count == 0)
		return -1.0;
	
	return sum / count;
}

Transaction const* registry_get_transaction(Registry const* registry, unsigned long id)
{
	assert(registry != NULL);
	return idmap_get(&registry->transactions_ids_map, id);
}
 
double registry_get_amount(Registry const* registry, unsigned long id)
{
	Transaction* transaction;

	assert(registry != NULL);

	transaction = idmap_get(&registry->transactions_ids_map, id);

	if (transaction == NULL)
		return -1.0;

	return transaction->amount;
}
 
TransactionStatus registry_get_status(Registry const* registry, unsigned long id)
{
	Transaction* transaction;

	assert(registry != NULL);

	transaction = idmap_get(&registry->transactions_ids_map, id);

	if (transaction == NULL)
		return 0;

	return transaction->status;
}

void registry_free(Registry* registry)
{
	assert(registry != NULL);
	
	free_users(registry);
	free_transactions(registry);
}

