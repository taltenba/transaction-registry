/**
 * \file
 * \brief   Transaction of the centralized transaction registry
 * \date    Created:  29/10/2017
 * \date    Modified: 03/01/2018
 * \author  ALTENBACH Thomas (thomas.altenbach@utbm.fr)
 * \author  GUERET Alexis (alexis.gueret@utbm.fr)
 */

#include <stdlib.h>
#include <stdio.h>
#include <transaction.h>
#include <assert.h>
#include <string.h>
#include <string_utils.h>
#include <openssl/rand.h>
#include <openssl/sha.h>

/**
 * \brief Number of random bytes used to generate the private key of a transaction
 */
#define PRIVATE_KEY_RND_BYTES 32

/**
 * \brief Time-to-live of a transaction, in seconds
 */
#define TIME_TO_LIVE 900

/**
 * \brief Go to the next token in the specified serialized string and abort execution by returning
 *        FALSE if there is no remaining token.
 * \param[in] str The string to cut the first time the macro is used, NULL afterwards
 * \param[out] token The variable in which to store the pointer to the beginning of the next token
 */
#define NEXT_TOKEN(str, token) do\
                               {\
                                   token = strtok(str, ";");\
                                   \
                                   if (token == NULL)\
                                       return FALSE;\
                               } while (0)

BOOL transaction_create(Transaction* transaction, unsigned long id, unsigned long recipient_id, double amount, unsigned long start_date_time)
{
	unsigned char rnd_bytes[PRIVATE_KEY_RND_BYTES];
	
	assert(transaction != NULL);
	assert(id != 0);
	assert(amount > 0.0);
	
	if (!RAND_bytes(rnd_bytes, PRIVATE_KEY_RND_BYTES))
		return FALSE;
	
	SHA256(rnd_bytes, PRIVATE_KEY_RND_BYTES, transaction->private_key);
	
    transaction->id = id;
    transaction->amount = amount;
    transaction->emitter_id = 0UL;
    transaction->recipient_id = recipient_id;
    transaction->status = PENDING;
    transaction->start_date_time = start_date_time;
    transaction->end_date_time = 0UL;
    transaction->ttl = TIME_TO_LIVE;
    
    return TRUE;
}

unsigned long transaction_get_id(Transaction const* transaction)
{
	assert(transaction != NULL);
    return transaction->id;
}

double transaction_get_amount(Transaction const* transaction)
{
	assert(transaction != NULL);
    return transaction->amount;
}

TransactionStatus transaction_get_status(Transaction const* transaction)
{
	assert(transaction != NULL);
    return transaction->status;
}

unsigned long transaction_get_recipient_id(Transaction const* transaction)
{
	assert(transaction != NULL);
	return transaction->recipient_id;
}

unsigned long transaction_get_emitter_id(Transaction const* transaction)
{
	assert(transaction != NULL);
	assert(transaction->status == CONFIRMED || transaction->status == EXECUTED);
	return transaction->emitter_id;
}

unsigned long transaction_get_start_date_time(Transaction const* transaction)
{
	assert(transaction != NULL);
    return transaction->start_date_time;
}

unsigned long transaction_get_end_date_time(Transaction const* transaction)
{
	assert(transaction != NULL);
	assert(transaction->status == EXECUTED || transaction->status == CANCELED);
    return transaction->end_date_time;
}

unsigned long transaction_get_ttl(Transaction const* transaction)
{
	assert(transaction != NULL);
    return transaction->ttl;
}

unsigned char const* transaction_get_private_key(Transaction const* transaction)
{
	assert(transaction != NULL);
	return transaction->private_key;
}

BOOL transaction_check_key(Transaction const* transaction, unsigned char const* key)
{
	assert(transaction != NULL);
	assert(key != NULL);
	
	return memcmp(key, transaction->private_key, SHA256_DIGEST_LENGTH) == 0;
}

BOOL transaction_is_expired(Transaction const* transaction, unsigned long cur_date)
{
	assert(transaction != NULL);
    return transaction->status == PENDING && cur_date > transaction->start_date_time + transaction->ttl;
}

void transaction_serialize(Transaction const* transaction, char* buffer)
{
	assert(transaction != NULL);
	assert(buffer != NULL);
	
	/* Since we compile in ANSI C, we sadly cannot use the far more secured snprintf */
	buffer += sprintf(buffer, "%lu;%d;%lu;%lu;%lu;%lu;%lu;%f;",
	                  transaction->id,
	                  (int) transaction->status,
	                  transaction->emitter_id,
	                  transaction->recipient_id,
	                  transaction->start_date_time,
	                  transaction->end_date_time,
	                  transaction->ttl,
	                  transaction->amount);
	
	print_transaction_key(transaction->private_key, buffer);
}     

BOOL transaction_deserialize(char* str, Transaction* transaction)
{
	char* token;
	
	assert(str != NULL);
	assert(transaction != NULL);
	
	NEXT_TOKEN(str, token);
	
	if (!parse_ulong(token, &transaction->id) || transaction->id == 0)
		return FALSE;
	
	NEXT_TOKEN(NULL, token);
	
	if (!parse_char(token, (char*) &transaction->status))
		return FALSE;
	
	NEXT_TOKEN(NULL, token);
	
	if (!parse_ulong(token, &transaction->emitter_id))
		return FALSE;
		
	NEXT_TOKEN(NULL, token);
	
	if (!parse_ulong(token, &transaction->recipient_id))
		return FALSE;
	
	NEXT_TOKEN(NULL, token);
	
	if (!parse_ulong(token, &transaction->start_date_time))
		return FALSE;
		
	NEXT_TOKEN(NULL, token);
	
	if (!parse_ulong(token, &transaction->end_date_time))
		return FALSE;
		
	NEXT_TOKEN(NULL, token);
	
	if (!parse_ulong(token, &transaction->ttl))
		return FALSE;
		
	NEXT_TOKEN(NULL, token);
	
	if (!parse_double(token, &transaction->amount) || transaction->amount <= 0.0)
		return FALSE;
	
	NEXT_TOKEN(NULL, token);
	
	return parse_transaction_key(token, transaction->private_key);
}

void print_transaction_status(TransactionStatus status, char* str)
{
	assert(str != NULL);
	
	switch (status)
	{
		case PENDING:
			strcpy(str, "PENDING");
			break;
		case CONFIRMED:
			strcpy(str, "CONFIRMED");
			break;
		case EXECUTED:
			strcpy(str, "EXECUTED");
			break;
		case CANCELED:
			strcpy(str, "CANCELED");
			break;
	}
}

void print_transaction_key(unsigned char const* key, char* str)
{
	int i;
	
	assert(key != NULL);
	assert(str != NULL);
	
	for (i = 0; i < SHA256_DIGEST_LENGTH; ++i)
		str += sprintf(str, "%.2x", key[i]);
}

BOOL parse_transaction_key(char const* str, unsigned char* key)
{
	int i = 0;
	char buffer[3];
	BOOL success = TRUE;
	
	buffer[2] = '\0';
	
	while (success && str[0] != '\0' && str[1] != '\0' && i < SHA256_DIGEST_LENGTH)
	{
		buffer[0] = str[0];
		buffer[1] = str[1];
		
		success = parse_uchar_hex(buffer, &key[i]);
		
		str += 2;
		++i;
	}
	
	return success && i == SHA256_DIGEST_LENGTH;
}
