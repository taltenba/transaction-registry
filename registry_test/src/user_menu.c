/**
 * \file
 * \brief   User menu of the registry testing application
 * \date    Created:  03/12/2017
 * \date    Modified: 03/01/2018
 * \author  ALTENBACH Thomas (thomas.altenbach@utbm.fr
 */
 
#include <app.h>
#include <menu.h>
#include <user_menu.h>
#include <user_table.h>
#include <transaction_table.h>
#include <string_utils.h>
#include <assert.h>

/**
 * \brief Number of entries in the menu
 */
#define MENU_ENTRIES_COUNT 11

/**
 * \brief Size of the buffer used to retrieve an ID from the user
 */
#define ID_BUFFER_SIZE 11

/**
 * \brief Size of the buffer used to retrieve a transaction amount from the user
 */
#define AMOUNT_BUFFER_SIZE 11

/**
 * \brief Size of the buffer used to retrieve a transaction key from the user
 */
#define TRANSACTION_KEY_BUFFER_SIZE 2 * SHA256_DIGEST_LENGTH + 1

/**
 * \enum MenuEntry
 * \brief Enumeration of the menu entries
 */
typedef enum 
{
	ENTRY_REQUEST_TRANSACTION = 0,
	ENTRY_CONFIRM_TRANSACTION = 1,
	ENTRY_EXECUTE_TRANSACTION = 2,
	ENTRY_CANCEL_TRANSACTION = 3,
	ENTRY_TRANSACTION_DETAILS = 4,
	ENTRY_TRANSACTION_LIST = 5,
	ENTRY_USER_LIST = 6,
	ENTRY_CHANGE_USER_NAME = 7,
	ENTRY_STATS = 8,
	ENTRY_BACK = 9,
	ENTRY_QUIT = 10
} MenuEntry;

/**
 * \brief Gets a transaction ID from the user
 * \param[in] context The app context
 * \param[out] id On success, the transaction ID
 * \return TRUE on success, FALSE otherwise (invalid input)
 *
 * On failure this function print an error message.
 */
static BOOL ask_transaction_id(AppContext* context, unsigned long* id)
{
	char buffer[ID_BUFFER_SIZE];
	
	app_ask(context, "Enter the transaction ID: ", ID_BUFFER_SIZE - 1, buffer);
	
	if (buffer[0] == '\0')
		return FALSE;
		
	if (!parse_ulong(buffer, id))
	{
		app_display_error(context, "ERROR: Invalid ID.");
		return FALSE;
	}
	
	return TRUE;
}

/**
 * \brief Performs a transaction request
 * \param[in] context The app context
 */
static void request_transaction(AppContext* context)
{
	double amount;
	unsigned long id;
	char buffer[AMOUNT_BUFFER_SIZE];
	char success_msg[63];
	
	app_ask(context, "Enter the amount of the transaction: ", AMOUNT_BUFFER_SIZE - 1, buffer);
	
	if (buffer[0] == '\0')
		return;
		
	if (!parse_double(buffer, &amount) || amount <= 0.0)
	{
		app_display_error(context, "ERROR: Invalid amount.");
		return;
	}
	
	id = registry_request_transaction(app_get_registry(context), app_get_user(context), amount);
	
	if (id == 0)
		app_display_error(context, "ERROR: Unable to create the transaction.");
	else
	{
		sprintf(success_msg, "New transaction request successfully created (ID = %lu)", id);
		app_display_popup_message(context, success_msg);
	}
}

/**
 * \brief Creates a string displaying the specified transaction key
 * \param[in] key The transaction key
 * \param[in] msg The message containing the transaction key (70-chars long, including the null
 *                terminating character).
 */
static void create_transaction_key_msg(unsigned char const* key, char* msg)
{
	msg += sprintf(msg, "Key: ");
	print_transaction_key(key, msg);
}

/**
 * \brief Confirms a transaction
 * \param[in] context The app context
 */
static void confirm_transaction(AppContext* context)
{
	unsigned long id;
	unsigned char const* transaction_key;
	char transaction_key_msg[70];
	
	if (!ask_transaction_id(context, &id))
		return;
	
	transaction_key = registry_confirm_transaction(app_get_registry(context),
	                                               app_get_user(context), id);
	
	if (transaction_key == NULL)
		app_display_error(context, "ERROR: Unable to confirm the transaction.");
	else
	{
		app_display_popup_message(context, "Transaction successfully confirmed.");
		
		create_transaction_key_msg(transaction_key, transaction_key_msg);
		app_display_popup_message(context, transaction_key_msg);
	}
}

/**
 * \brief Executes a transaction
 * \param[in] context The app context
 */
static void execute_transaction(AppContext* context)
{
	unsigned long id;
	unsigned char key[SHA256_DIGEST_LENGTH];
	char key_str[TRANSACTION_KEY_BUFFER_SIZE];
	
	if (!ask_transaction_id(context, &id))
		return;
		
	app_ask(context, "Key: ", TRANSACTION_KEY_BUFFER_SIZE - 1, key_str);
	
	if (!parse_transaction_key(key_str, key))
	{
		app_display_error(context, "ERROR: Invalid transaction key.");
		return;
	}
	
	if (!registry_execute_transaction(app_get_registry(context), app_get_user(context), id, key))
		app_display_error(context, "ERROR: Unable to execute the transaction.");
	else
		app_display_popup_message(context, "Transaction successfully executed.");
}

/**
 * \brief Cancels a transaction
 * \param[in] context The app context
 */
static void cancel_transaction(AppContext* context)
{
	unsigned long id;
	
	if (!ask_transaction_id(context, &id))
		return;
		
	if (!registry_cancel_transaction(app_get_registry(context), id))
		app_display_error(context, "ERROR: Unable to cancel the transaction.");
	else
		app_display_popup_message(context, "Transaction successfully canceled.");
}

/**
 * \brief Displays the details of a transaction
 * \param[in] context The app context
 */
static void display_transaction(AppContext* context)
{
	WINDOW* win;
	Transaction const* transaction;
	TransactionStatus status;
	unsigned long id;
	unsigned long date_time;
	unsigned char const* key;
	char buffer[65];
	
	if (!ask_transaction_id(context, &id))
		return;
		
	transaction = registry_get_transaction(app_get_registry(context), id);
	
	if (transaction == NULL)
	{
		app_display_error(context, "ERROR: The specified transaction ID doesn't exist.");
		return;
	}
	
	app_set_screen_title(context, "Transaction details");
	
	win = app_get_main_win(context);
	
	werase(win);
	wmove(win, 0, 0);
	
	wprintw(win, "\t%-20s:\t%lu\n", "ID", id);
	wprintw(win, "\t%-20s:\t%.2f\n", "Amount", transaction_get_amount(transaction));
	
	status = transaction_get_status(transaction);
	
	print_transaction_status(status, buffer);
	wprintw(win, "\t%-20s:\t%s\n", "Status", buffer);

	wprintw(win, "\t%-20s:\t%lu\n", "Recipient ID", transaction_get_recipient_id(transaction));
	
	if (status == CONFIRMED || status == EXECUTED)
		wprintw(win, "\t%-20s:\t%lu\n", "Emitter ID", transaction_get_emitter_id(transaction));
	
	date_time = transaction_get_start_date_time(transaction);
	print_date_time(date_time, buffer);
	
	wprintw(win, "\t%-20s:\t%s\n", "Start date", buffer);
	
	if (status == PENDING)
		wprintw(win, "\t%-20s:\t%lu seconds\n", "Time-to-live", transaction_get_ttl(transaction));
	else if (status != CONFIRMED)
	{
		date_time = transaction_get_end_date_time(transaction);
		print_date_time(date_time, buffer);
		wprintw(win, "\t%-20s:\t%s\n", "End date", buffer);
	}
	
	if ((status == CONFIRMED || status == EXECUTED) &&
	    user_get_wrapped_id(app_get_user(context)) == transaction_get_emitter_id(transaction))
	{
		key = transaction_get_private_key(transaction);
		print_transaction_key(key, buffer);
		wprintw(win, "\t%-20s:\t%s\n", "Private key", buffer);
	}
	
	wrefresh(win);
	
	app_display_popup_message(context, "Press any key to go back.");
}

/**
 * \brief Changes the name of the current user
 * \param[in] context The app context
 */
static void change_user_name(AppContext* context)
{
	char buffer[USER_MAX_NAME_LENGTH + 1];
	
	app_ask(context, "Enter the new user name:", USER_MAX_NAME_LENGTH, buffer);
	
	if (buffer[0] == '\0')
		return;
	
	if (!user_set_name(user_get_wrapped_user(app_get_user(context)), buffer))
		app_display_error(context, "ERROR: Invalid name.");
}

/**
 * \brief Prints a duration in a string (days, hours, minutes and seconds)
 * \param[in] dur The duration in seconds
 * \param[out] str The string containing the duration
 */
static void print_duration(unsigned long dur, char* str)
{
	int secs, mins, hours;
	unsigned long days;
	
	secs = dur % 60;
	dur /= 60;
	mins = dur % 60;
	dur /= 60;
	hours = dur % 24;
	days = dur / 24;
	
	sprintf(str, "%lud %02dh %02dm %02ds", days, hours, mins, secs);
}

/**
 * \brief Displays the statistics of the registry and the current user
 * \param[in] context The app context
 */
static void display_stats(AppContext* context)
{
	WINDOW* win;
	unsigned long start_date_time;
	unsigned long end_date_time;
	double average_transac_time;
	char buffer[32];
	
	if (!app_get_date_time_limits(context, &start_date_time, &end_date_time))
		return;
	
	app_set_screen_title(context, "Statistics");
	
	win = app_get_main_win(context);
	
	werase(win);
	
	print_date_time(start_date_time, buffer);
	wprintw(win, " %-11s: %s\n", "Start date", buffer);
	
	print_date_time(end_date_time, buffer);
	wprintw(win, " %-11s: %s\n", "End date", buffer);
	
	waddstr(win, "\n REGISTRY:\n");
	
	average_transac_time = registry_get_average_transaction_time(app_get_registry(context), 
	                                                             start_date_time, end_date_time);
	
	if (average_transac_time < 0)
	{
		wprintw(win, " \t%-25s: %s\n", "Average transaction time", "No finished transaction");
	}
	else
	{
		print_duration((unsigned long) average_transac_time, buffer);
		wprintw(win, " \t%-25s: %s\n", "Average transaction time", buffer);
	}
	                           
	wprintw(win, "\n USER:\n");
	
	wprintw(win, " \t%-25s: %10.2f\n", "Average credit",
	        user_get_average_credit(app_get_user(context), start_date_time, end_date_time));
	        
	wprintw(win, " \t%-25s: %10.2f\n", "Average debit",
	        user_get_average_debit(app_get_user(context), start_date_time, end_date_time));
	
	wrefresh(win);
	
	app_display_popup_message(context, "Press any key to go back.");
}

BOOL goto_user_menu(AppContext* context)
{
	char const* const menu_entries[MENU_ENTRIES_COUNT] = 
	            {"Request transaction", "Confirm transaction", "Execute transaction", 
	             "Cancel transaction", "Transaction details", "Transaction list", "User list",
	             "Change user name", "Statistics", "Back", "Quit"};
	
	Menu menu;
	MenuEntry chosen_entry;
	BOOL exit;
	
	assert(context != NULL);
	assert(app_has_user(context));
	
	menu_init(&menu, MENU_ENTRIES_COUNT, menu_entries);
	
	exit = FALSE;
	
	do
	{
		app_set_screen_title(context, "User menu");
		menu_display(&menu, context);
		
		chosen_entry = menu_wait_for_input(&menu, context);
		
		switch(chosen_entry)
		{
			case ENTRY_REQUEST_TRANSACTION:
				request_transaction(context);
				break;
			case ENTRY_CONFIRM_TRANSACTION:
				confirm_transaction(context);
				break;
			case ENTRY_EXECUTE_TRANSACTION:
				execute_transaction(context);
				break;
			case ENTRY_CANCEL_TRANSACTION:
				cancel_transaction(context);
				break;
			case ENTRY_TRANSACTION_DETAILS:
				display_transaction(context);
				break;
			case ENTRY_TRANSACTION_LIST:
				goto_transaction_table(context);
				break;
			case ENTRY_USER_LIST:
				goto_user_table(context);
				break;
			case ENTRY_CHANGE_USER_NAME:
				change_user_name(context);
				break;
			case ENTRY_STATS:
				display_stats(context);
				break;
			case ENTRY_BACK:
			case ENTRY_QUIT:
				exit = TRUE;
				break;
		}
	} while (!exit);
	
	return chosen_entry == ENTRY_QUIT;
}
