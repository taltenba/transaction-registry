/**
 * \file
 * \brief   Transaction table of the registry testing application
 * \date    Created:  04/12/2017
 * \date    Modified: 03/01/2018
 * \author  ALTENBACH Thomas (thomas.altenbach@utbm.fr
 */

#include <assert.h>
#include <app.h>
#include <transaction.h>
#include <transactions_it.h>
#include <table.h>
#include <transaction_table.h>
#include <string_utils.h>
#include <ncurses.h>

/**
 * \brief Prints the header line of the table
 * \param[in] win The header window
 */
static void print_header_line(WINDOW* win)
{
	mvwprintw(win, 0, 0, "\t%-10s\t%-10s\t%-10s\t%-20s\t%-20s\t%-10s", "ID", "STATUS", "AMOUNT", 
	                     "START", "END", "TTL");
}

/**
 * \brief Prints a line in the table
 * \param[in] context The app context
 * \param[in] win The table content window
 * \param[in] e The element to print
 * \param[in] line The current line
 */
static void print_line(AppContext* context, WINDOW* win, void* e, int line)
{
	double amount;
	char status_str[MAX_TRANSACTION_STATUS_LENGTH + 1];
	char start_date_str[20];
	char end_date_str[20];
	char ttl_str[10];
	TransactionStatus status;
	Transaction* transaction = (Transaction*) e;
	
	status = transaction_get_status(transaction);
	amount = transaction_get_amount(transaction);
	
	print_transaction_status(status, status_str);
	print_date_time(transaction_get_start_date_time(transaction), start_date_str);
	
	if ((status == CONFIRMED || status == EXECUTED) && 
	    user_get_wrapped_id(app_get_user(context)) == transaction_get_emitter_id(transaction))
	{
		amount = -amount;
	}
	
	if (status == EXECUTED || status == CANCELED)
		print_date_time(transaction_get_end_date_time(transaction), end_date_str);
	else
		end_date_str[0] = '\0';
	
	if (status == PENDING)
		sprintf(ttl_str, "%lu", transaction_get_ttl(transaction));
	else
		ttl_str[0] = '\0';
	
	mvwprintw(win, line, 0, "\t%10lu\t%-10s\t%10.2f\t%-20s\t%-20s\t%-10s", 
	                        transaction_get_id(transaction),
	                        status_str,
	                        amount,
	                        start_date_str,
	                        end_date_str,
	                        ttl_str);
}

/**
 * \brief Indicates if the specified iterator have a next element
 * \param[in] it The iterator
 * \return TRUE if the iterator have a next element, FALSE otherwise
 */
static BOOL it_has_next(void* it)
{
	return transactions_it_has_next((TransactionsIterator*) it);
}

/**
 * \brief Indicates if the specified iterator have a previous element
 * \param[in] it The iterator
 * \return TRUE if the iterator have a previous element, FALSE otherwise
 */
static BOOL it_has_prev(void* it)
{
	return transactions_it_has_prev((TransactionsIterator*) it);
}

/**
 * \brief Gets the next element in the iteration
 * \param[in] it The iterator
 * \return The next element in the iteration
 */
static void* it_next(void* it)
{
	return transactions_it_next((TransactionsIterator*) it);
}

/**
 * \brief Gets the previous element in the iteration
 * \param[in] it The iterator
 * \return The previous element in the iteration
 */
static void* it_prev(void* it)
{
	return transactions_it_prev((TransactionsIterator*) it);
}

void goto_transaction_table(AppContext* context)
{
	Table table;
	TransactionsIterator it;
	unsigned long start_date_time;
	unsigned long end_date_time;
	
	assert(context != NULL);
	assert(app_has_user(context));
	
	if (!app_get_date_time_limits(context, &start_date_time, &end_date_time))
		return;
		
	app_set_screen_title(context, "Transaction list");
	
	user_get_history_limit(app_get_user(context), start_date_time, end_date_time, &it);
	
	table_init(&table, &it, it_has_next, it_has_prev, it_next, it_prev, print_line, print_header_line);
	table_display(&table, context);
}
