/**
 * \file
 * \brief   Main function of the registry test application
 * \date    Created:  02/12/2017
 * \date    Modified: 03/01/2018
 * \author  ALTENBACH Thomas (thomas.altenbach@utbm.fr
 */

#include <stdio.h>
#include <stdlib.h>
#include <app.h>
#include <main_menu.h>
#include <time.h>
#include <openssl/sha.h>
#include <openssl/evp.h>
#include <openssl/err.h>
#include <openssl/conf.h>

/**
 * \brief Initializes the OpenSSL library
 */
static void init_openssl()
{
	ERR_load_crypto_strings();
    OpenSSL_add_all_algorithms();
    CONF_modules_load(NULL, NULL, 0UL);
}

/**
 * \brief Free all the memory related to the OpenSSL library
 */
static void free_openssl()
{
	EVP_cleanup();
    CRYPTO_cleanup_all_ex_data();
    ERR_free_strings();
}

int main(int argc, char** argv)
{
	AppContext context;

	if (argc < 2)
	{
		puts("ERROR: Please provide the location of the registry file.");
		return EXIT_FAILURE;
	}
	
	if (argc > 3)
	{
		puts("ERROR: Too many arguments provided.");
		return EXIT_FAILURE;
	}
	
	init_openssl();
	
	srand(time(NULL));
	
	if (!app_init(&context, argv[1]))
		return EXIT_FAILURE;
	
	if (argc == 3)
		app_load_user(&context, argv[2]);
	
	goto_main_menu(&context);
	
	app_save_registry(&context, argv[1]);
	
	app_free(&context);
	free_openssl();

    return EXIT_SUCCESS;
}

