/**
 * \file
 * \brief   User table of the registry testing application
 * \date    Created:  03/12/2017
 * \date    Modified: 03/01/2018
 * \author  ALTENBACH Thomas (thomas.altenbach@utbm.fr
 */

#include <app.h>
#include <user.h>
#include <users_it.h>
#include <table.h>
#include <user_table.h>
#include <ncurses.h>
#include <assert.h>

/**
 * \brief Prints the header line of the table
 * \param[in] win The header window
 */
static void print_header_line(WINDOW* win)
{
	mvwprintw(win, 0, 0, "\t%-10s\t%-*s", "ID", USER_MAX_NAME_LENGTH, "NAME");
}

/**
 * \brief Prints a line in the table
 * \param[in] context The app context
 * \param[in] win The table content window
 * \param[in] e The element to print
 * \param[in] line The current line
 */
static void print_line(AppContext* context, WINDOW* win, void* e, int line)
{
	User* user = (User*) e;
	
	mvwprintw(win, line, 0, "\t%10lu\t%-*s", user_get_id(user), USER_MAX_NAME_LENGTH, user_get_name(user));
}

/**
 * \brief Indicates if the specified iterator have a next element
 * \param[in] it The iterator
 * \return TRUE if the iterator have a next element, FALSE otherwise
 */
static BOOL it_has_next(void* it)
{
	return users_it_has_next((UsersIterator*) it);
}

/**
 * \brief Indicates if the specified iterator have a previous element
 * \param[in] it The iterator
 * \return TRUE if the iterator have a previous element, FALSE otherwise
 */
static BOOL it_has_prev(void* it)
{
	return users_it_has_prev((UsersIterator*) it);
}

/**
 * \brief Gets the next element in the iteration
 * \param[in] it The iterator
 * \return The next element in the iteration
 */
static void* it_next(void* it)
{
	return users_it_next((UsersIterator*) it);
}

/**
 * \brief Gets the previous element in the iteration
 * \param[in] it The iterator
 * \return The previous element in the iteration
 */
static void* it_prev(void* it)
{
	return users_it_prev((UsersIterator*) it);
}

void goto_user_table(AppContext* context)
{
	Table table;
	UsersIterator it;
	
	assert(context != NULL);
	
	app_set_screen_title(context, "User list");
	
	registry_get_user_list(app_get_registry(context), &it);
	
	table_init(&table, &it, it_has_next, it_has_prev, it_next, it_prev, print_line, print_header_line);
	
	table_display(&table, context);
}
