/**
 * \file
 * \brief   Generic routines for displaying a menu
 * \date    Created:  03/12/2017
 * \date    Modified: 03/01/2018
 * \author  ALTENBACH Thomas (thomas.altenbach@utbm.fr
 */
 
#include <stdlib.h>
#include <app.h>
#include <ncurses.h>
#include <menu.h>
#include <assert.h>
#include <string.h>
 
void menu_init(Menu* menu, size_t entries_count, char const* const* entries)
{
	size_t i;
	size_t len;
	size_t max_entry_length;
	
	assert(menu != NULL);
	assert(entries != NULL);
	assert(entries_count > 0);
	
	menu->cur_entry = 0;
	menu->entries_count = entries_count;
	menu->entries = entries;
	
	max_entry_length = 0;
	
	for (i = 0; i < entries_count; ++i)
	{
		len = strlen(entries[i]);
		
		if (len > max_entry_length)
			max_entry_length = len;
	}
	
	menu->max_entry_length = max_entry_length;
}

void menu_display(Menu* menu, AppContext* context)
{
	WINDOW* win;
	size_t i;
	int x_max, y_max;
	int x, y;
	
	assert(menu != NULL);
	assert(context != NULL);
	
	win = app_get_main_win(context);
	
	werase(win);
	
	getmaxyx(win, y_max, x_max);
	
	x = (x_max - menu->max_entry_length - 4) / 2;
	y = (y_max - menu->entries_count) / 2;
	
	wmove(win, y, x);
	
	for (i = 0; i < menu->entries_count; ++i)
	{
		if (i == menu->cur_entry)
			wattron(win, A_STANDOUT);
		else
			wattroff(win, A_STANDOUT);
			
		mvwprintw(win, y + i, x, "%2lu. %-*s", i + 1, menu->max_entry_length, menu->entries[i]);
	}
	
	wrefresh(win);
	
	app_display_message(context, "UP: Move up    DOWN: Move down    ENTER: Select");
}

int menu_wait_for_input(Menu* menu, AppContext* context)
{
	WINDOW* win;
	size_t cur_entry;
	int input;
	int x_max, y_max;
	int x, y;
	
	assert(menu != NULL);
	assert(context != NULL);
	
	win = app_get_main_win(context);
	
	getmaxyx(win, y_max, x_max);
	
	x = (x_max - menu->max_entry_length - 4) / 2;
	y = (y_max - menu->entries_count) / 2;
	
	cur_entry = menu->cur_entry;
	input = getch();
	
	while (input != KEY_ENTER && input != '\n')
	{
		mvwprintw(win, y + cur_entry, x, "%2lu. %-*s", cur_entry + 1, menu->max_entry_length, menu->entries[cur_entry]);
		
		if (input == KEY_UP)
			cur_entry = cur_entry == 0 ? menu->entries_count - 1 : cur_entry - 1;
		else if (input == KEY_DOWN)
			cur_entry = (cur_entry + 1) % menu->entries_count;
		
		wattron(win, A_STANDOUT);
		mvwprintw(win, y + cur_entry, x, "%2lu. %-*s", cur_entry + 1, menu->max_entry_length, menu->entries[cur_entry]);
		wattroff(win, A_STANDOUT);
		
		wrefresh(win);
		
		input = getch();
	}

	menu->cur_entry = cur_entry;
	
	return cur_entry;
}
