/**
 * \file
 * \brief   Generic routines for displaying a table
 * \date    Created:  03/12/2017
 * \date    Modified: 03/01/2018
 * \author  ALTENBACH Thomas (thomas.altenbach@utbm.fr
 */
 
#include <ctype.h>
#include <bool.h>
#include <ncurses.h>
#include <table.h>
#include <ctype.h>
#include <assert.h>

/**
 * \brief Prints the next page of the table
 * \param[in] table The table
 * \param[in] context The app context
 * \param[in] content_win The content window of the table
 * \param[in] dir_changed TRUE iff the last table move was backwards
 */
static void print_next_page(Table* table, AppContext* context, WINDOW* content_win, BOOL dir_changed)
{
	int max_y;
	int i;
	max_y = getmaxy(content_win);
	
	if (dir_changed && table->it_has_next(table->it))
	{
		i = 0;
		
		while (i < max_y && table->it_has_next(table->it))
		{
			table->it_next(table->it);
			++i;
		}
	}
	
	if (!table->it_has_next(table->it))
		return;
	
	werase(content_win);

	i = 0;
	
	while (i < max_y && table->it_has_next(table->it))
	{
		table->print_line(context, content_win, table->it_next(table->it), i);
		++i;
	}
	
	wrefresh(content_win);
}

/**
 * \brief Prints the previous page of the table
 * \param[in] table The table
 * \param[in] context The app context
 * \param[in] content_win The content window of the table
 * \param[in] dir_changed TRUE iff the last table move was forwards
 */
static void print_prev_page(Table* table, AppContext* context, WINDOW* content_win, BOOL dir_changed)
{
	int max_y;
	int i;
	
	max_y = getmaxy(content_win);
	
	if (dir_changed && table->it_has_prev(table->it))
	{
		for (i = getcury(content_win); i >= 0; --i)
			table->it_prev(table->it);
	}
	
	if (!table->it_has_prev(table->it))
		return;

	werase(content_win);

	i = max_y - 1;
	
	while (i >= 0 && table->it_has_prev(table->it))
	{
		table->print_line(context, content_win, table->it_prev(table->it), i);
		--i;
	}
	
	wrefresh(content_win);
}

/**
 * \brief Handles the user inputs
 * \param[in] table The table
 * \param[in] context The app context
 * \param[in] content_win The content window of the table
 */
static void handle_inputs(Table* table, AppContext* context, WINDOW* content_win)
{
	int input;
	BOOL last_was_forwards = TRUE;    /* TRUE iff the last move was forwards */
	
	input = toupper(getch());
	
	while (input != 'B')
	{
		if (input == KEY_LEFT)
		{
			print_prev_page(table, context, content_win, last_was_forwards);
			last_was_forwards = FALSE;
		}
		else if (input == KEY_RIGHT)
		{
			print_next_page(table, context, content_win, !last_was_forwards);
			last_was_forwards = TRUE;
		}
				
		input = toupper(getch());
	}
}
 
void table_init(Table* table, void* it, BOOL (*it_has_next)(void*), BOOL (*it_has_prev)(void*), 
                void* (*it_next)(void*), void* (*it_prev)(void*), 
                void (*print_line)(AppContext*, WINDOW*, void*, int), void (*print_header_line)(WINDOW*))
{
	assert(table != NULL);
	assert(it != NULL);
	assert(it_has_next != NULL);
	assert(it_has_prev != NULL);
	assert(it_next != NULL);
	assert(it_prev != NULL);
	assert(print_line != NULL);
	assert(print_header_line != NULL);
	
	table->it = it;
	table->it_has_next = it_has_next;
	table->it_has_prev = it_has_prev;
	table->it_next = it_next;
	table->it_prev = it_prev;
	table->print_line = print_line;
	table->print_header_line = print_header_line;
}

void table_display(Table* table, AppContext* context)
{
	WINDOW* main_win;
	WINDOW* header_win;
	WINDOW* content_win;
	int beg_x, beg_y;
	int max_x, max_y;
	
	assert(table != NULL);
	assert(context != NULL);
	
	main_win = app_get_main_win(context);
	
	werase(main_win);
	getbegyx(main_win, beg_y, beg_x);
	getmaxyx(main_win, max_y, max_x);
	
	header_win = derwin(main_win, 2, max_x - beg_x, 0, 0);
	content_win = derwin(main_win, (max_y - beg_y) - 2, max_x - beg_x, 2, 0);
	
	wrefresh(main_win);
	
	table->print_header_line(header_win);
	mvwhline(header_win, 1, 0, 0, max_x - beg_x);
	wrefresh(header_win);
	
	app_display_message(context, "LEFT: Previous page    RIGHT: Next page    B: Back");
	
	print_next_page(table, context, content_win, FALSE);
	
	handle_inputs(table, context, content_win);
	
	delwin(header_win);
	delwin(content_win);
}
