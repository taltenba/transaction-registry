/**
 * \file
 * \brief   Main menu of the registry testing application
 * \date    Created:  03/12/2017
 * \date    Modified: 03/01/2018
 * \author  ALTENBACH Thomas (thomas.altenbach@utbm.fr
 */
 
#include <app.h>
#include <menu.h>
#include <main_menu.h>
#include <user_table.h>
#include <assert.h>
#include <user_menu.h>

/**
 * \brief Number of entries in the menu
 */
#define MENU_ENTRIES_COUNT 4

/**
 * \brief Size the buffer used to get a user ID
 */
#define ID_BUFFER_SIZE 11

/**
 * \enum MenuEntry
 * \brief Enumeration of the menu entries
 */
typedef enum 
{
	ENTRY_LOG_IN = 0,
	ENTRY_NEW_USER = 1,
	ENTRY_USER_LIST = 2,
	ENTRY_QUIT = 3
} MenuEntry;

/**
 * \brief Logs in a user
 * \param[in] context The app context
 */
static void log_in(AppContext* context)
{
	char buffer[ID_BUFFER_SIZE];
	
	app_ask(context, "Enter your user ID: ", ID_BUFFER_SIZE - 1, buffer);
	
	if (buffer[0] == '\0')
		return;
	
	app_load_user(context, buffer);
}

/**
 * \brief Creates a new user
 * \param[in] context The app context
 */
static void new_user(AppContext* context)
{
	char buffer[USER_MAX_NAME_LENGTH + 1];
	
	app_ask(context, "Enter the name of the user: ", USER_MAX_NAME_LENGTH, buffer);
	
	if (buffer[0] == '\0')
		return;
	
	app_create_user(context, buffer);
}

void goto_main_menu(AppContext* context)
{
	char const* const menu_entries[MENU_ENTRIES_COUNT] = 
	                               {"Log in", "New user", "User list", "Quit"};
	Menu menu;
	BOOL exit;
	MenuEntry chosen_entry;
	
	assert(context != NULL);
	
	menu_init(&menu, MENU_ENTRIES_COUNT, menu_entries);
	
	exit = FALSE;
	
	do
	{
		if (app_has_user(context))
		{
			exit = goto_user_menu(context);
			app_unload_user(context);
		}
		else
		{
			app_set_screen_title(context, "Main menu");
			menu_display(&menu, context);
	
			chosen_entry = menu_wait_for_input(&menu, context);
			
			switch(chosen_entry)
			{
				case ENTRY_LOG_IN:
					log_in(context);
					break;
				case ENTRY_NEW_USER:
					new_user(context);
					break;
				case ENTRY_USER_LIST:
					goto_user_table(context);
					break;
				case ENTRY_QUIT:
					exit = TRUE;
					break;
			}
		}
	} while (!exit);
}
