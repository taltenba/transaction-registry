/**
 * \file
 * \brief   Core routines of the registry testing application
 * \date    Created:  02/12/2017
 * \date    Modified: 03/01/2018
 * \author  ALTENBACH Thomas (thomas.altenbach@utbm.fr
 */

#include <app.h>
#include <assert.h>
#include <limits.h>
#include <ctype.h>
#include <string.h>
#include <string_utils.h>

/**
 * \brief Height of the top bar showing the screen title,
 *        the balance of the logged in user and the last transaction date.
 */
#define TOP_BAR_HEIGHT 1

/**
 * \brief Height of the bottom bar used to display messages to the user
 */
#define STATUS_BAR_HEIGHT 1

/**
 * \brief The index of the color pair used for the top and status bars
 */
#define COLOR_PAIR_BAR 1

/**
 * \brief The index of the color pair used for displaying error messages
 */
#define COLOR_PAIR_ERROR 2

/**
 * \brief Size of the buffer used to get a date time
 */
#define DATE_BUFFER_SIZE 20

/**
 * \brief Size of the buffer used to get a path
 */
#define REGISTRY_PATH_BUFFER_SIZE 96

/**
 * \brief Flushes the content of stdin
 */
static void flush_stdin()
{
	int c;
	
	nodelay(stdscr, TRUE);
	
	do 
	{ 
		c = getch();
	} while (c != ERR);
	
	nodelay(stdscr, FALSE);
}

/**
 * \brief Initializes the user interface
 * \param[in] context The app context
 */
static void init_interface(AppContext* context)
{
	initscr();
	
	getmaxyx(stdscr, context->old_term_lines, context->old_term_cols);
	system("resize -s 28 120 > /dev/null");
	
	endwin();
	refresh();
	
	cbreak();
	noecho();
	keypad(stdscr, TRUE);
	curs_set(0);
	start_color();
	use_default_colors();
	
	init_pair(COLOR_PAIR_BAR, COLOR_BLACK, COLOR_WHITE);
	init_pair(COLOR_PAIR_ERROR, COLOR_RED, COLOR_WHITE);
	
	context->top_bar = newwin(TOP_BAR_HEIGHT, COLS, 0, 0);
	context->main_win = newwin(LINES - TOP_BAR_HEIGHT - STATUS_BAR_HEIGHT, COLS, TOP_BAR_HEIGHT + 1, 0);
	context->status_bar = newwin(STATUS_BAR_HEIGHT, COLS, LINES - STATUS_BAR_HEIGHT, 0);
	
	refresh();
	
	wbkgd(context->top_bar, COLOR_PAIR(COLOR_PAIR_BAR));
	wbkgd(context->status_bar, COLOR_PAIR(COLOR_PAIR_BAR));
	wrefresh(context->top_bar);
	wrefresh(context->status_bar);
	
	flush_stdin();
}

/**
 * \brief Tries to load a registry from the specified file and show an appropriate error message 
 *        on failure.
 * \param[in] path The path of the registry file
 * \param[out] registry The registry
 * \return TRUE on success, FALSE otherwise
 */
static BOOL init_registry(AppContext* context, char const* registry_path)
{
	app_display_message(context, "Loading registry file...");
	
	switch (registry_load(registry_path, &context->registry))
	{
		case REGLOAD_SUCCESS:
			return TRUE;
		case REGLOAD_FILE_NOT_FOUND:
			app_display_popup_message(context, "No file found at the specified location. A random registry will be initialized.");
			
			app_display_message(context, "Initializing a new random registry...");
			
			if (!registry_init_random(&context->registry))
			{
				app_display_error(context, "ERROR: Out of memory");
				return FALSE;
			}
			
			app_display_popup_message(context, "Initialization completed. Press any key to continue...");
			return TRUE;
		case REGLOAD_INVALID_FILE:
			app_display_error(context, "ERROR: The specified file is not a valid registry file.");
			return FALSE;
		case REGLOAD_OUT_OF_MEMORY:
			app_display_error(context, "ERROR: Out of memory");
			return FALSE;
	}
	
	return FALSE;
}

BOOL app_init(AppContext* context, char const* registry_path)
{
	assert(context != NULL);
	assert(registry_path != NULL);
	
	init_interface(context);
	
	if (!init_registry(context, registry_path))
	{
		endwin();
		return FALSE;
	}
	
	context->has_user = FALSE;
	
	return TRUE;
}

Registry* app_get_registry(AppContext* context)
{
	assert(context != NULL);
	return &context->registry;
}

WINDOW* app_get_main_win(AppContext const* context)
{
	assert(context != NULL);
	return context->main_win;
}

BOOL app_has_user(AppContext const* context)
{
	assert(context != NULL);
	return context->has_user;
}

LoadedUser* app_get_user(AppContext* context)
{
	assert(context != NULL);
	assert(context->has_user);
	return &context->user;
}

void app_display_message(AppContext* context, char const* msg)
{
	assert(context != NULL);
	assert(msg != NULL);
	
	werase(context->status_bar);
	mvwaddstr(context->status_bar, 0, 0, msg);
	wrefresh(context->status_bar);
}

void app_display_popup_message(AppContext* context, char const* msg)
{
	assert(context != NULL);
	assert(msg != NULL);
	
	app_display_message(context, msg);
	
	curs_set(1);
	getch();
	curs_set(0);
	
	werase(context->status_bar);
}

void app_display_error(AppContext* context, char const* error)
{
	assert(context != NULL);
	assert(error != NULL);
	
	wattron(context->status_bar, COLOR_PAIR(COLOR_PAIR_ERROR) | A_BOLD);
	app_display_popup_message(context, error);
	wattroff(context->status_bar, COLOR_PAIR(COLOR_PAIR_ERROR) | A_BOLD);
}

void app_ask(AppContext* context, char const* question, size_t max_char, char* input)
{
	assert(context != NULL);
	assert(question != NULL);
	assert(input != NULL);
	
	app_display_message(context, question);
	
	curs_set(1);
	echo();
	
	wgetnstr(context->status_bar, input, max_char);
	
	noecho();
	curs_set(0);
}

/**
 * \brief Gets a date time from the user
 * \param[in] context The app context
 * \param[in] msg The message to display to the user
 * \param[in] default_value The default value to use if the user inputs an emptry string
 * \param[out] date_time On success, the timestamp in seconds corresponding to the date time input by the user
 * \return TRUE on success, FALSE otherwise (invalid input)
 *
 * On failure this function prints an error message.
 */
static BOOL get_date_time(AppContext* context, char const* msg, unsigned long default_value, unsigned long* date_time)
{
	time_t tmp;
	char buffer[DATE_BUFFER_SIZE];
	
	app_ask(context, msg, DATE_BUFFER_SIZE - 1, buffer);
	
	if (buffer[0] == '\0')
	{
		*date_time = default_value;
		return TRUE;
	}
	
	if (!parse_date_time(buffer, &tmp))
	{
		app_display_error(context, "ERROR: Invalid date time format");
		return FALSE;
	}
	else if (tmp < 0)
	{
		app_display_error(context, "ERROR: Invalid date.");
		return FALSE;
	}
	
	*date_time = tmp;
	
	return TRUE;
}

BOOL app_get_date_time_limits(AppContext* context, unsigned long* start, unsigned long* end)
{
	BOOL success;
	
	assert(context != NULL);
	assert(start != NULL);
	assert(end != NULL);
	
	
	success = get_date_time(context, "Start date time (YYYY-MM-DD HH:MM:SS, can be left empty): ", 0, start) &&
	          get_date_time(context, "End date time (YYYY-MM-DD HH:MM:SS, can be left empty): ", time(NULL), end);
	          
	if (!success)
		return FALSE;
		
	if (*end < *start)
	{
		app_display_error(context, "ERROR: Start must be before end.");
		return FALSE;
	}
	
	return TRUE;
}

/**
 * \brief Refreshes the top bar content
 * \param[in] app The app context
 */
static void refresh_top_bar(AppContext* context)
{
	char date_str[20];
	
	werase(context->top_bar);
	
	print_date_time(registry_get_last_transaction_date(&context->registry), date_str);
	mvwprintw(context->top_bar, 0, 0, "Last transaction: %s", date_str);
	
	mvwaddstr(context->top_bar, 0, (COLS - strlen(context->screen_title)) / 2, context->screen_title);
	
	if (context->has_user)
	{
		mvwprintw(context->top_bar, 0, COLS - 36, "ID: %-10lu  Balance: %-10.2f", 
		           user_get_wrapped_id(&context->user), user_get_balance(&context->user));
	}
	
	wrefresh(context->top_bar);
}

void app_set_screen_title(AppContext* context, char const* title)
{
	assert(context != NULL);
	assert(title != NULL);
	
	strncpy(context->screen_title, title, APP_MAX_SCREEN_TITLE_LENGHT + 1);
	refresh_top_bar(context);
}

/**
 * \brief Loads the user with the specified ID
 * \param[in] context The app context
 * \param[in] user_id The ID of the user
 *
 * On failure an error message is displayed.
 */
static void load_user(AppContext* context, unsigned long user_id)
{
	if (context->has_user)
		registry_unload_user(&context->user);
	
	context->has_user = registry_load_user(&context->registry, user_id, &context->user);
	
	if (!context->has_user)
	{
		app_display_error(context, "ERROR: Unable to load the user (invalid user id or out of memory).");
		return;
	}
	
	refresh_top_bar(context);
}

void app_create_user(AppContext* context, char const* name)
{
	User* user;
	char success_msg[70];
	
	assert(context != NULL);
	assert(name != NULL);
	
	user = registry_create_user(&context->registry, name);

	if (user == NULL)
	{
		app_display_error(context, "ERROR: Unable to create the new user (invalid name or out of "
		                           "memory).");
	}
	else
	{
		sprintf(success_msg, "User successfully created (ID = %lu). Press any key to log in.", 
		                     user->id);
		app_display_popup_message(context, success_msg);
		load_user(context, user->id);
	}
}

void app_load_user(AppContext* context, char const* user_id_str)
{
	unsigned long user_id;
	
	assert(context != NULL);
	assert(user_id_str != NULL);
	
	if (parse_ulong(user_id_str, &user_id))
		load_user(context, user_id);
	else
		app_display_error(context, "ERROR: Invalid user id.");
}

void app_unload_user(AppContext* context)
{
	assert(context != NULL);
	
	if (!context->has_user)
		return;
		
	registry_unload_user(&context->user);
	context->has_user = FALSE;
	refresh_top_bar(context);
}

void app_save_registry(AppContext* context, char const* path)
{
	char path_buffer[REGISTRY_PATH_BUFFER_SIZE];
	char exit_buffer[2];
	
	assert(context != NULL);
	assert(path != NULL);
	
	app_display_message(context, "Saving registry...");
	
	if (registry_save(&context->registry, path))
		return;
		
	exit_buffer[0] = '\0';
	
	do
	{
		app_display_error(context, "ERROR: Unable to save the registry.");
		app_ask(context, "New save location: ", REGISTRY_PATH_BUFFER_SIZE - 1, path_buffer);
		
		if (path_buffer[0] == '\0')
		{
			app_ask(context, "Registry not saved, do you really want to exit ? (y/n) ", 1, exit_buffer);
			exit_buffer[0] = toupper(exit_buffer[0]);
		}
	} while (exit_buffer[0] != 'Y' && !registry_save(&context->registry, path_buffer));
}

void app_free(AppContext* context)
{
	char buffer[32];
	
	assert(context != NULL);
	
	if (context->has_user)
		registry_unload_user(&context->user);
		
	registry_free(&context->registry);
	
	sprintf(buffer, "resize -s %d %d > /dev/null", context->old_term_lines, context->old_term_cols);
	system(buffer);
	
	endwin();
}
