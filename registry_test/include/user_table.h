/**
 * \file
 * \brief   User table of the registry testing application
 * \date    Created:  03/12/2017
 * \date    Modified: 03/01/2018
 * \author  GUERET Alexis (alexis.gueret@utbm.fr)
 */

#ifndef USER_TABLE_H
#define USER_TABLE_H

#include <app.h>

/**
 * \brief Displays the user table of the application
 * \param[in] context The app context
 */
void goto_user_table(AppContext* context);

#endif    /* USER_TABLE_H */
