/**
 * \file
 * \brief   Transaction table of the registry testing application
 * \date    Created:  04/12/2017
 * \date    Modified: 03/01/2018
 * \author  GUERET Alexis (alexis.gueret@utbm.fr)
 */

#ifndef TRANSACTION_TABLE_H
#define TRANSACTION_TABLE_H

#include <app.h>

/**
 * \brief Displays the transaction table of the application
 * \param[in] context The app context
 */
void goto_transaction_table(AppContext* context);

#endif    /* TRANSACTION_TABLE_H */
