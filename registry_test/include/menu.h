/**
 * \file
 * \brief   Generic routines for displaying a menu
 * \date    Created:  02/12/2017
 * \date    Modified: 01/01/2018
 * \author  GUERET Alexis (alexis.gueret@utbm.fr)
 */

#ifndef MENU_H
#define MENU_H

#include <stdlib.h>
#include <app.h>

/**
 * \struct Menu
 * \brief A menu of the application
 */
typedef struct
{
	size_t cur_entry;            /**< The currently selected entry */
	size_t entries_count;        /**< The number of entries */
	size_t max_entry_length;     /**< The length of the longest entry */
	char const* const* entries;  /**< The names of the entries */
} Menu;

/**
 * \brief Initializes a new menu without displaying it
 * \param[out] menu The menu to initialize
 * \param[in] entries_count The number of menu entries (> 0)
 * \param[in] entries The names of each entry
 *
 * Only a reference to the entry names is kept by the menu.
 */
void menu_init(Menu* menu, size_t entries_count, char const* const* entries);

/**
 * \brief Displays a menu
 * \param[in] menu The menu
 * \param[in] context The app context
 */
void menu_display(Menu* menu, AppContext* context);

/**
 * \brief Waits until the user chooses a menu entry
 * \param[in] menu The menu
 * \param[in] context The app context
 * \return The index of the chosen entry
 */
int menu_wait_for_input(Menu* menu, AppContext* context);

#endif    /* MENU_H */
