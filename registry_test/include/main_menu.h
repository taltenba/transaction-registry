/**
 * \file
 * \brief   Main menu of the registry testing application
 * \date    Created:  03/12/2017
 * \date    Modified: 03/01/2018
 * \author  GUERET Alexis (alexis.gueret@utbm.fr)
 */

#ifndef MAIN_MENU_H
#define MAIN_MENU_H

#include <app.h>

/**
 * \brief Displays the main menu of the application
 * \param[in] context The app context
 */
void goto_main_menu(AppContext* context);

#endif    /* MAIN_MENU_H */
