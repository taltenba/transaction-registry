/**
 * \file
 * \brief   User menu of the registry testing application
 * \date    Created:  03/12/2017
 * \date    Modified: 03/01/2018
 * \author  GUERET Alexis (alexis.gueret@utbm.fr)
 */

#ifndef USER_MENU_H
#define USER_MENU_H

#include <app.h>

/**
 * \brief Displays the user menu of the application
 * \param[in] context The app context
 * \return TRUE if the user wants to exit the app, FALSE otherwise
 *
 * The application context must contains a loaded user.
 */
BOOL goto_user_menu(AppContext* context);

#endif    /* USER_MENU_H */
