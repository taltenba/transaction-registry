/**
 * \file
 * \brief   Core routines of the registry testing application
 * \date    Created:  02/12/2017
 * \date    Modified: 03/01/2018
 * \author  GUERET Alexis (alexis.gueret@utbm.fr)
 */

#ifndef APP_H
#define APP_H

#include <bool.h>
#include <ncurses.h>
#include <registry.h>

/**
 * \brief Maximum length of a screen title
 */
#define APP_MAX_SCREEN_TITLE_LENGHT 31

/**
 * \struct AppContext
 * \brief The current context of the application
 */
typedef struct
{
	WINDOW* top_bar;                                      /* Top bar of the screen */
	WINDOW* main_win;                                     /* Main window of the screen */
	WINDOW* status_bar;                                   /* Bottom bar of the screen */
	Registry registry;                                    /* The registry */
	LoadedUser user;                                      /* The loaded user if any */
	BOOL has_user;                                        /* TRUE iff a user is loaded */
	char screen_title[APP_MAX_SCREEN_TITLE_LENGHT + 1];   /* Title of the current screen */
	int old_term_lines;                                   /* Number of lines in the terminal before the app initialization */
	int old_term_cols;                                    /* Number of columns in the terminal before the app initialization */
} AppContext;

/**
 * \brief Initializes the application
 * \param[out] context The application context to initialized
 * \param[in] registry_path The path of the registry
 * \return TRUE on success, FALSE otherwise (out of memory or invalid registry file)
 *
 * If no file is found at the specified location, a new random registry will be initialized.
 */
BOOL app_init(AppContext* context, char const* registry_path);

/**
 * \brief Gets the loaded registry
 * \param[in] context The app context
 * \return The loaded registry
 */
Registry* app_get_registry(AppContext* context);

/**
 * \brief Gets the main window of the application
 * \param[in] context The app context
 * \return The main window
 */
WINDOW* app_get_main_win(AppContext const* context);

/**
 * \brief Indicates whether or not a user is currently loaded
 * \param[in] context The app context
 * \return TRUE if a user is currently loaded, FALSE otherwise
 */
BOOL app_has_user(AppContext const* context);

/**
 * \brief Gets the loaded user
 * \param[in] context The app context, having a loaded user
 * \return The loaded user
 *
 * A user must be currently loaded.
 */
LoadedUser* app_get_user(AppContext* context);

/**
 * \brief Displays a permanent message to the user in the status bar
 * \param[in] context The app context
 * \param[in] msg The message to display
 */
void app_display_message(AppContext* context, char const* msg);

/**
 * \brief Displays a pop-up message to the user in the status bar
 * \param[in] context The app context
 * \param[in] msg The message to display
 */
void app_display_popup_message(AppContext* context, char const* msg);

/**
 * \brief Displays an error message to the user in the status bar
 * \param[in] context The app context
 * \param[in] error The error message to display
 */
void app_display_error(AppContext* context, char const* error);

/**
 * \brief Asks a question to the user and gets its answer
 * \param[in] context The app context
 * \param[in] question The question to display
 * \param[in] max_char The maximum number of characters to get from the user (excluding the null
 *                     terminating character).
 * \param[out] input The answer of the user
 */
void app_ask(AppContext* context, char const* question, size_t max_char, char* input);

/**
 * \brief Gets a start and end date time from the user
 * \param[in] context The app context
 * \param[out] start The start date time
 * \param[out] end The end date time
 * \return TRUE on success, FALSE otherwise (invalid date time or start > end)
 *
 * An error message is displayed on failure.
 */
BOOL app_get_date_time_limits(AppContext* context, unsigned long* start, unsigned long* end);

/**
 * \brief Sets the current screen title
 * \param[in] context The app context
 * \param[in] title The title (its length must be lower or equals than APP_MAX_SCREEN_TITLE_LENGHT)
 * 
 * The title is displayed in the top bar.
 */
void app_set_screen_title(AppContext* context, char const* title);

/**
 * \brief Creates a new user and loads it on success
 * \param[in] context The app context
 * \param[in] name The name of the new user
 *
 * If another user is currently loaded, the latter is unloaded.
 */
void app_create_user(AppContext* context, char const* name);

/**
 * \brief Loads the user having the specified ID
 * \param[in] context The app context
 * \param[in] user_id_str A string containing the user ID
 *
 * The string is parsed and an error message is displayed on failure.
 * If another user is currently loaded, the latter is unloaded.
 */
void app_load_user(AppContext* context, char const* user_id_str);

/**
 * \brief Frees all the memory related to the loaded user
 * \param[in] context The app context
 *
 * If no user is currently loaded, nothing is done.
 */
void app_unload_user(AppContext* context);

/**
 * \brief Saves the loaded registry in a file
 * \param[in] context The app context
 * \param[in] path The path where the registry must be saved
 *
 * On failure, this function ask the user for a new location until success or
 * the user want to exit even so.
 */
void app_save_registry(AppContext* context, char const* path);

/**
 * \brief Frees all the memory related to the application
 * \param[in] context The app context
 */
void app_free(AppContext* context);

#endif    /* APP_H */
