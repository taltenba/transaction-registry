/**
 * \file
 * \brief   Generic routines for displaying a table
 * \date    Created:  03/12/2017
 * \date    Modified: 03/01/2018
 * \author  GUERET Alexis (alexis.gueret@utbm.fr)
 */

#ifndef TABLE_H
#define TABLE_H

#include <bool.h>
#include <app.h>
#include <ncurses.h>

/**
 * \struct Table
 * \brief A table
 */
typedef struct
{
	void* it;                                               /**< Iterator iterating through the elements of the table */
	BOOL (*it_has_next)(void*);                             /**< Returns TRUE iff the specified iterator has a next element */
	BOOL (*it_has_prev)(void*);                             /**< Returns TRUE iff the specified iterator has a previous element */
	void* (*it_next)(void*);                                /**< Returns the next element of the specified iterator */
	void* (*it_prev)(void*);                                /**< Returns the previous element of the specified iterator */
	void (*print_line)(AppContext*, WINDOW*, void*, int);   /**< Prints the specified element in the given window and at the given line index */
	void (*print_header_line)(WINDOW*);                     /**< Prints the header of the table in the specified window */
} Table;

/**
 * \brief Initializes a new table without displaying it
 * \param[out] table The table to initialize
 * \param[in] it_has_next Returns TRUE iff the specified iterator has a next element
 * \param[in] it_has_prev Returns TRUE iff the specified iterator has a previous element 
 * \param[in] it_next Returns the next element of the specified iterator
 * \param[in] it_prev Returns the previous element of the specified iterator
 * \param[in] print_line Prints the specified element in the given window and at the given line index
 * \param[in] print_header_line Prints the header of the table in the specified window 
 *
 * Only a reference to the specified iterator is kept by the table.
 */
void table_init(Table* table, void* it, BOOL (*it_has_next)(void*), BOOL (*it_has_prev)(void*), 
                void* (*it_next)(void*), void* (*it_prev)(void*), 
                void (*print_line)(AppContext*, WINDOW*, void*, int), void (*print_header_line)(WINDOW*));

/**
 * \brief Displays a table in the main window of the app and handles user input
 * \param[in] table The table
 * \param[in] context The app context
 *
 * This function returns when the user press 'B' so as to exit the table.
 */
void table_display(Table* table, AppContext* context);

#endif    /* TABLE_H */
