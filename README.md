# Transaction Registry
This project has been developed in 2017 as part of my studies, when I was in second year of preparatory class. The goal was to implement in C a centralized and secured transaction registry, the role of which is to keep the track of monetary exchanges between different actors and to secure the transactions. The transactions are secured in the sense the registry is intended to prevent a transaction emitter from giving an invalid payment solution to the transaction recipient by ensuring the former has a sufficient balance to complete the transaction. More details about the project topic can be found in the `subject.pdf` file.

In addition to create the registry library itself, a simple CLI application using `ncurses` has been developed in order to be able to easily test the library.

![Image Imgur](https://i.imgur.com/eM3Pbci.png)

## Technologies
* C
* ncurses

## Getting started
### Prerequisites
* Linux
* GCC
* ncurses library
* OpenSSL library

### Building
To build the program, simply run the makefile:
```sh
make
```

### Running
After building, the test application can then be runned using `registry_test/run.sh`:
```sh
cd registry_test
sh run.sh
```

## Gallery
<img src="https://i.imgur.com/Zgdbr3C.png" width="33%"/>
<img src="https://i.imgur.com/C88A1LD.png" width="33%"/>
<img src="https://i.imgur.com/eM3Pbci.png" width="33%"/>

## Authors
* ALTENBACH Thomas - @taltenba
